package ru.kiseru.homework;

import java.util.Scanner;

public class Task009 {

    public static void main(String[] args) {
        System.out.println(computeResult(readDoubleFromConsole()));
    }

    private static double readDoubleFromConsole() {
        try (var scanner = new Scanner(System.in)) {
            return scanner.nextDouble();
        }
    }

    private static double computeResult(double x) {
        if (x > 2) {
            return (x * x - 1) / (x + 2);
        }

        if (x > 0) {
            return (x * x - 1) * (x + 2);
        }

        return x * x * (1 + 2 * x);
    }
}
