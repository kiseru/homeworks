package ru.kiseru.homework;

import java.util.Scanner;
import java.util.stream.DoubleStream;

public class Task012 {

    public static void main(String[] args) {
        System.out.println(computeResult(readInt()));
    }

    private static int readInt() {
        try (var scanner = new Scanner(System.in)) {
            return scanner.nextInt();
        }
    }

    private static double computeResult(int n) {
        return 1.0 - DoubleStream.iterate(3, i -> i < n, i -> i + 2)
                .map(i -> 1.0 / i * i)
                .sum();
    }
}
