package ru.kiseru.homework;

import java.util.Scanner;

public class Task014 {

    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            var n = scanner.nextInt();
            var x = scanner.nextDouble();
            System.out.format("%.2f", func(Math.toRadians(x), n));
        }
    }

    private static double func(double x, int n) {
        if (n == 1) {
            return Math.cos(x);
        }

        return Math.cos(x + func(x, n - 1));
    }
}
