package ru.kiseru.homework;

import java.util.Scanner;

public class Task016 {

    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            var n = scanner.nextInt();
            var x = scanner.nextDouble();
            var a = 1;
            var result = 0;
            for (int i = 1; i <= n; i++) {
                a *= x + i;
                result += a;
            }
            System.out.println(result);
        }
    }
}
