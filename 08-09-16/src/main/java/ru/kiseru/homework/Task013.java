package ru.kiseru.homework;

import java.util.Scanner;

public class Task013 {

    public static void main(String[] args) {
        var n = readInt();
        var result = 1.0;
        for (var i = 1; i <= n; i++) {
            result *= 2 * n;
            result /= 2 * n - 1;
            result *= 2 * n;
            result /= 2 * n + 1;
        }

        System.out.println(result);
    }

    private static int readInt() {
        try (var scanner = new Scanner(System.in)) {
            return scanner.nextInt();
        }
    }
}
