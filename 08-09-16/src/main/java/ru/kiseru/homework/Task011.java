package ru.kiseru.homework;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Task011 {

    public static void main(String[] args) {
        var n = readInt();
        System.out.println(computeFactorialFrom(getFactorialStartValue(n), n));
    }

    private static int readInt() {
        try (var scanner = new Scanner(System.in)) {
            return scanner.nextInt();
        }
    }

    private static int getFactorialStartValue(int n) {
        return (n & 1) != 0 ? 1 : 2;
    }

    private static int computeFactorialFrom(int startValue, int n) {
        return IntStream.iterate(startValue, i -> i < n, i -> i + 2)
                .reduce(1, (acc, cur) -> acc * cur);
    }
}
