package ru.kiseru.homework;

import java.util.Scanner;

public class Task010 {

    public static void main(String[] args) {
        System.out.println(computeResult(readDoubleFromConsole()));
    }

    private static double readDoubleFromConsole() {
        try (var scanner = new Scanner(System.in)) {
            return scanner.nextDouble();
        }
    }

    private static double computeResult(double x) {
        return x > 2 ? (x * x - 1) / (x + 2) : x > 0 ? (x * x - 1) * (x + 2) : x * x * (1 + 2 * x);
    }
}
