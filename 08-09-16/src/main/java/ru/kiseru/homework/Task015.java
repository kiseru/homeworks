package ru.kiseru.homework;

import java.util.Scanner;

public class Task015 {

    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            var n = scanner.nextInt();
            var x = scanner.nextDouble();
            var result = x / (n + x);
            for (var i = n - 1; i >= 1; i--) {
                result = i + x / result;
            }
            System.out.print(result);
        }
    }
}
