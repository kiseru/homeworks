package ru.kiseru.homework;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Task017 {

    public static void main(String[] args) {
        var n = readInt();
        var result = IntStream.iterate(1, i -> i <= n, i -> i + 1)
                .mapToDouble(i -> ((double) sqr(factorial(i - 1))) / factorial(i << 1))
                .sum();
        System.out.println(result);
    }

    private static int readInt() {
        try (var scanner = new Scanner(System.in)) {
            return scanner.nextInt();
        }
    }

    private static int factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }

        return IntStream.iterate(2, i -> i <= n, i -> i + 1)
                .reduce(1, (acc, cur) -> acc * cur);
    }

    private static int sqr(int x) {
        return x * x;
    }
}
