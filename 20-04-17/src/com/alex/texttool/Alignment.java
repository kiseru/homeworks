package com.alex.texttool;

enum Alignment {
    RIGHT, LEFT, CENTER, WIDTH
}
