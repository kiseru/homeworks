package com.alex.texttool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class TextAlignment {
    private final int width;
    private final File file;
    private final Alignment alignment;
    private final File to;

    public TextAlignment(File file, Alignment alignment, int width) {
        this.to = new File("test.txt");
        this.file = file;
        this.alignment = alignment;
        this.width = width;
    }

    public void run() throws IOException {
        String text = readFile();
        String[] lines = text.split("[\t]");

        if (alignment == Alignment.LEFT) {
            text = makeLeftAlignment(lines);
            writeFile(text);
        } else if (alignment == Alignment.RIGHT) {
            text = makeRightAlignment(lines);
            writeFile(text);
        }
    }

    private String makeRightAlignment(String[] lines) {
        String result = "";

        for (int i = 0; i < lines.length; i++) {
            String line = "    ";
            String[] words = lines[i].split("[\n ]");

            int currentWidth = line.length();
            for (int j = 0; j < words.length; j++) {
                if (currentWidth + words[j].length() > width) {
                    line += "\n";
                    currentWidth = 0;
                }


                line += words[j] + " ";
                currentWidth += words[j].length() + 1;
            }

            while (currentWidth < width) {
                line = " " + line;
                currentWidth++;
            }

            result += line + "\n";
        }

        return result;
    }

    private String makeLeftAlignment(String[] lines) {
        String result = "";

        for (int i = 0; i < lines.length; i++) {
            String line = "\t";
            String[] words = lines[i].split("[\n ]");

            int currentWidth = line.length();
            for (int j = 0; j < words.length; j++) {
                if (currentWidth + words[j].length() > width) {
                    line += "\n";
                    currentWidth = 0;
                }

                line += words[j] + " ";
                currentWidth += words[j].length() + 1;
            }

            result += line + "\n";
        }

        return result;
    }

    private String readFile() throws IOException {
        FileChannel fileReadChannel = new FileInputStream(file).getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int) fileReadChannel.size());

        String result = "";

        while (fileReadChannel.read(byteBuffer) != -1) {
            byteBuffer.flip();
            result += new String(byteBuffer.array(), "UTF-8");
            byteBuffer.clear();
        }

        return result;
    }

    private void writeFile(String text) throws IOException {
        FileChannel fileWriteChannel = new FileOutputStream(to).getChannel();
        ByteBuffer buffer = ByteBuffer.wrap(text.getBytes());
        fileWriteChannel.write(buffer);
    }
}
