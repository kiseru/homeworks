package com.alex.texttool;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

class TextAlignmentTest {

    @Test
    void testLeftAlignment() throws IOException {
        new TextAlignment(new File("book.txt"), Alignment.LEFT, 121).run();
    }

    @Test
    void testRightAlignment() throws IOException {
        new TextAlignment(new File("book.txt"), Alignment.RIGHT, 121).run();
    }
}