package patterns.bridge;

public class SonyRemoteControl extends RemoteControl {
    public SonyRemoteControl(Televisor televisor) {
        super(televisor);
    }

    @Override
    public void on() {
        this.getTelevisor().on();
    }

    @Override
    public void off() {
        this.getTelevisor().off();
    }

    @Override
    public void setChannel(int id) {
        this.getTelevisor().tuneChannel(id);
    }
}