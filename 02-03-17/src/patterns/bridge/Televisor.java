package patterns.bridge;

public interface Televisor {
    void on();
    void off();
    void tuneChannel(int id);
    void addChannel(Channel channel);
}
