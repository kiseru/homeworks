package patterns.bridge;

public abstract class RemoteControl {
    private Televisor televisor;

    public RemoteControl(Televisor televisor) {
        this.televisor = televisor;
    }

    public abstract void on();

    public abstract void off();

    public abstract void setChannel(int id);

    public Televisor getTelevisor() {
        return televisor;
    }
}