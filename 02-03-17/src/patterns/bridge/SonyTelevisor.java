package patterns.bridge;

import java.util.LinkedList;

public class SonyTelevisor implements Televisor {
    private LinkedList<Channel> channels;
    private Channel currentChannel;

    public SonyTelevisor() {
        this.channels = new LinkedList<>();
    }

    @Override
    public void on() {
        System.out.println("Sony TV is on");
    }

    @Override
    public void off() {
        System.out.println("Sony TV is off");
    }

    @Override
    public void tuneChannel(int id) {
        try {
            this.currentChannel = this.channels.get(id - 1);
            System.out.println("Channel is got");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void addChannel(Channel channel) {
        this.channels.add(channel);
    }
}