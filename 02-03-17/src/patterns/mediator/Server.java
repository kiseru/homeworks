package patterns.mediator;

public interface Server {
    void notify(String message);
}