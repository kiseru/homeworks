package patterns.mediator;

public class User extends AbstractUser {
    private String name;

    public User(Server server, String name) {
        super(server);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(String message) {
        System.out.println(name + " gets message: " + message);
    }
}