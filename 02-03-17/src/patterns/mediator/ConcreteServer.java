package patterns.mediator;

import java.util.LinkedList;

public class ConcreteServer implements Server {
    private LinkedList<User> users;

    public ConcreteServer() {
        users = new LinkedList<>();
    }

    public void addUser(User user) {
        users.add(user);
        System.out.println(user.getName() + " join the conversation");
    }

    public void removeUser(User user) {
        if (users.remove(user)) {
            System.out.println(user.getName() + " leave the conversation");
        } else {
            System.out.println(user.getName() + " is not founded in the conversation");
        }
    }

    @Override
    public void notify(String message) {
        for (User user : users) {
            user.update(message);
        }
    }
}