package patterns.mediator;

public abstract class AbstractUser {
    private Server server;

    public AbstractUser(Server server) {
        this.server = server;
    }

    public void send(String message) {
        this.server.notify(message);
    }

    public abstract void update(String message);
}