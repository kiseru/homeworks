package patterns.flyweight;

public class TreeBranch {
    private int branchNumber;

    public TreeBranch(int branchNumber) {
        this.branchNumber = branchNumber;
    }

    public void hang(Lamp lamp) {
        System.out.println("Hang " + lamp.getColour() + " lamp on branch " + branchNumber);
    }
}