package patterns.flyweight;

import java.util.HashMap;
import java.util.LinkedList;

public class LampFactory {
    private HashMap<String, Lamp> lamps;

    public LampFactory() {
        lamps = new HashMap<>();
    }

    public Lamp findLamp(String colour) {
        Lamp lamp;
        if (lamps.containsKey(colour)) {
            lamp = lamps.get(colour);
        } else {
            lamp = new Lamp(colour);
            lamps.put(colour, lamp);
        }

        return lamp;
    }

    public int size() {
        return lamps.size();
    }
}