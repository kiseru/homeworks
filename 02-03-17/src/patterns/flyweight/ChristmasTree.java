package patterns.flyweight;

public class ChristmasTree {
    private LampFactory lampFactory;
    private int lampsHung;

    public ChristmasTree() {
        lampFactory = new LampFactory();
        lampsHung = 0;
    }

    public void hangLamp(String colour, int branchNumber) {
        new TreeBranch(branchNumber).hang(lampFactory.findLamp(colour));
        lampsHung++;
    }

    public void dressUpTree() {
        hangLamp("red", 1);
        hangLamp("green", 1);
        hangLamp("blue", 2);
    }
}