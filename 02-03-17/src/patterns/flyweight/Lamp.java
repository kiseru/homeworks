package patterns.flyweight;

public class Lamp {
    private String colour;

    public Lamp(String colour) {
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }
}