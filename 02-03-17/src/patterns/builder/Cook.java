package patterns.builder;

public class Cook {
    private BreakfestBuilder breakfastBuilder;

    public void setBreakfastBuilder(BreakfestBuilder breakfastBuilder) {
        this.breakfastBuilder = breakfastBuilder;
    }

    public Breakfast getBreakfast() {
        return breakfastBuilder.getBreakfast();
    }

    public void buildBreakfast() {
        this.breakfastBuilder.makeBreakfast();
        this.breakfastBuilder.makeBeverage();
        this.breakfastBuilder.makeMeal();
    }
}