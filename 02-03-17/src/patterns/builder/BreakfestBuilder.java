package patterns.builder;

public abstract class BreakfestBuilder {
    protected Breakfast breakfast;

    public Breakfast getBreakfast() {
        return this.breakfast;
    }

    public void makeBreakfast() {
        this.breakfast = new Breakfast();
    }

    public abstract void makeBeverage();

    public abstract void makeMeal();
}