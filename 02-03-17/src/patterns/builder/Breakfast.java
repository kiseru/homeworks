package patterns.builder;

public class Breakfast {
    private String beverage;
    private String meal;

    public void setBeverage(String beverage) {
        this.beverage = beverage;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }
}