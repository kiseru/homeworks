package patterns.builder;

public class SimpleBreakfastBuilder extends BreakfestBuilder {
    @Override
    public void makeBeverage() {
        this.breakfast.setBeverage("Tea");
    }

    @Override
    public void makeMeal() {
        this.breakfast.setMeal("Sandwich");
    }
}