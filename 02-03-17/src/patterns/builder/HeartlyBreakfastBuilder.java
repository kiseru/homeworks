package patterns.builder;

public class HeartlyBreakfastBuilder extends BreakfestBuilder {
    @Override
    public void makeBeverage() {
        this.breakfast.setBeverage("Coffee");
    }

    @Override
    public void makeMeal() {
        this.breakfast.setMeal("Bacon and eggs");
    }
}