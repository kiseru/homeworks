package patterns.command.light;

public class Light {
    private boolean isOn;

    public Light() {}

    public void on() {
        isOn = true;
    }

    public void off() {
        isOn = false;
    }
}