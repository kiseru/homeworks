package patterns.command;

public class RemoteControl {
    private Command[] onCommands;
    private Command[] offCommands;
    private CommandStorage undoCommands;

    public RemoteControl(int size) {
        undoCommands = new CommandStorage();

        this.onCommands = new Command[size];
        this.offCommands = new Command[size];

        for (int id = 0; id < size; id++) {
            onCommands[id] = NoCommand.getCommand();
            offCommands[id] = NoCommand.getCommand();
        }
    }

    public void addCommand(int id, Command commandOn, Command commandOff) {
        onCommands[id] = commandOn;
        offCommands[id] = commandOff;
    }

    public void removeCommand(int id) {
        onCommands[id] = NoCommand.getCommand();
        offCommands[id] = NoCommand.getCommand();
    }

    public void pushButtonOn(int id) {
        onCommands[id].execute();
        undoCommands.push(onCommands[id]);
    }

    public void pushButtonOff(int id) {
        offCommands[id].execute();
        undoCommands.push(offCommands[id]);
    }

    public void pushUndoButton() {
        undoCommands.pop().undo();
    }
}