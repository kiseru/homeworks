package patterns.command;

public class NoCommand implements Command {
    private static Command command;

    private NoCommand() {}

    public static Command getCommand() {
        if (command == null) {
            synchronized (NoCommand.class) {
                if (command == null) {
                    command = new NoCommand();
                }
            }
        }

        return command;
    }

    @Override
    public void execute() {
        System.out.println("None");
    }

    @Override
    public void undo() {
        System.out.println("None");
    }
}