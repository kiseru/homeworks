package patterns.command;

import java.util.LinkedList;

public class CommandStorage {
    private LinkedList<Command> list;
    private int size;

    public CommandStorage() {
        list = new LinkedList<>();
        size = 0;
    }

    public void push(Command command) {
        list.add(command);
    }

    public Command pop() {
        if (size == 0) return NoCommand.getCommand();
        return list.removeFirst();
    }
}