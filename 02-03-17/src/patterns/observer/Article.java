package patterns.observer;

public class Article {
    private String author;
    private String data;
    private String article;

    public Article(String author, String data, String article) {
        this.author = author;
        this.data = data;
        this.article = article;
    }

    public void show() {
        System.out.println(article);
        System.out.println(author + " " + data);
    }
}