package patterns.observer;

import java.util.LinkedList;

public class Blog implements IBlog {
    private String name;
    private LinkedList<IFollower> followers;
    private Article lastArticle;

    public Blog(String name) {
        this.name = name;
        this.followers = new LinkedList<>();
    }

    @Override
    public void addFollower(IFollower follower) {
        this.followers.add(follower);
        System.out.println(follower.getLogin() + " subscribed on " + name);
    }

    @Override
    public void deleteFollower(IFollower follower) {
        this.followers.remove(follower);
        System.out.println(follower.getLogin() + " unsubscribe from " + name);
    }

    @Override
    public void notifyFollowers() {
        for (IFollower follower : followers) {
            follower.update(lastArticle);
        }
    }

    @Override
    public void addArticle(Article article) {
        this.lastArticle = article;
        notifyFollowers();
    }
}