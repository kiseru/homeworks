package patterns.observer;

public interface IFollower {
    void update(Article article);
    String getLogin();
}
