package patterns.observer;

public interface IBlog {
    void addFollower(IFollower follower);
    void deleteFollower(IFollower follower);
    void notifyFollowers();
    void addArticle(Article article);
}