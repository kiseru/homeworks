package patterns.observer;

import patterns.observer.*;

public class Test {
    public static void main(String[] args) {
        IBlog javaBlog = new Blog("Java blog");
        IBlog rubyBlog = new Blog("Ruby blog");

        IFollower alex = new Follower("Alex");
        IFollower abstractFollower = new Follower("Abstract Follower");

        javaBlog.addFollower(alex);
        javaBlog.addFollower(abstractFollower);

        rubyBlog.addFollower(alex);

        javaBlog.addArticle(new Article("Airat Faridovich", "21.02.2017", "Java is cool!"));

        rubyBlog.addArticle(new Article("FlatStack", "07.08.2017", "Ruby is cool!"));
    }
}