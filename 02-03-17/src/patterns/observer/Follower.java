package patterns.observer;

public class Follower implements IFollower {
    private String login;

    public Follower(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public void update(Article article) {
        System.out.println(login + ": ");
        article.show();
    }
}