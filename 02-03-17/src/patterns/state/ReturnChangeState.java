package patterns.state;

public class ReturnChangeState implements State {
    private final CoffeeMachine coffeeMachine;
    private final float change;

    public ReturnChangeState(CoffeeMachine coffeeMachine, float change) {
        this.coffeeMachine = coffeeMachine;
        this.change = change;
    }

    @Override
    public void handle() throws InterruptedException {
        System.out.println("Take your change, please!");
        System.out.println("Your change: " + change);
        Thread.sleep(5000);
        coffeeMachine.setState(new MakingCoffeeState(coffeeMachine));
    }
}