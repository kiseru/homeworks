package patterns.state;

public interface State {
    void handle() throws InterruptedException;
}