package patterns.state;

public class NoCoffeeState implements State {
    private CoffeeMachine coffeeMachine;

    public NoCoffeeState(CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
    }

    @Override
    public void handle() throws InterruptedException {
        System.out.println("Coffee Machine is empty");
        System.out.println("Need to fill it");
        Thread.sleep(5000);
        fill();
        Thread.sleep(5000);
        coffeeMachine.setState(new NotEnoughMoneyState(coffeeMachine));
    }

    private void fill() throws InterruptedException {
        coffeeMachine.setGramsOfCoffee(500);
        System.out.println("The machine is being filled");
        Thread.sleep(5000);
        System.out.println("Ready to work");
    }
}