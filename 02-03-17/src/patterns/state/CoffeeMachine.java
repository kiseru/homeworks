package patterns.state;

public class CoffeeMachine {
    private State state;
    private float coffeePrice;
    private float gramsOfCoffee;

    public CoffeeMachine() {
        state = new NotEnoughMoneyState(this);
        gramsOfCoffee = 15;
        coffeePrice = 50;
    }

    public void setState(State state) {
        this.state = state;
    }

    public float getCoffeePrice() {
        return coffeePrice;
    }

    public float getGramsOfCoffee() {
        return gramsOfCoffee;
    }

    public void setGramsOfCoffee(float gramsOfCoffee) {
        this.gramsOfCoffee = gramsOfCoffee;
    }

    public void run() throws InterruptedException {
        System.out.println("Coffee machine is up!");
        Thread.sleep(5000);
        while (true) {
            state.handle();
        }
    }
}