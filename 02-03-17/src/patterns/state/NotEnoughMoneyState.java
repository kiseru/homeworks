package patterns.state;

import java.util.Scanner;

public class NotEnoughMoneyState implements State {
    private CoffeeMachine coffeeMachine;
    private float currentMoney;

    public NotEnoughMoneyState(CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
        this.currentMoney = 0;
    }

    public void setCurrentMoney(float currentMoney) {
        this.currentMoney = currentMoney;
    }

    @Override
    public void handle() throws InterruptedException {
        System.out.println("Coffee's price is " + coffeeMachine.getCoffeePrice());
        System.out.println("Current money: " + currentMoney);
        Thread.sleep(5000);
        if (currentMoney < coffeeMachine.getCoffeePrice()) {
            System.out.print("Please input money: ");
            float inputtedMoney = new Scanner(System.in).nextFloat();
            this.setCurrentMoney(currentMoney + inputtedMoney);
            Thread.sleep(3000);
        } else {
            coffeeMachine.setState(new EnoughMoneyState(coffeeMachine, currentMoney));
        }

    }
}