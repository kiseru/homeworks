package patterns.state;

import java.util.Scanner;

public class EnoughMoneyState implements State {
    private CoffeeMachine coffeeMachine;
    private float currentMoney;

    public EnoughMoneyState(CoffeeMachine coffeeMachine, float currentMoney) {
        this.coffeeMachine = coffeeMachine;
        this.currentMoney = currentMoney;
    }

    @Override
    public void handle() throws InterruptedException {
        String answer;
        do {
            System.out.print("Do you want to get coffee? (Y/N): ");
            answer = new Scanner(System.in).next();

        } while (!answer.toUpperCase().equals("Y") && !answer.toUpperCase().equals("N"));

        Thread.sleep(5000);
        if (answer.toUpperCase().equals("Y")) {
            coffeeMachine.setState(new ReturnChangeState(coffeeMachine, currentMoney - coffeeMachine.getCoffeePrice()));
        } else {
            coffeeMachine.setState(new CancellationState(coffeeMachine, currentMoney));
        }
    }
}