package patterns.state;

public class CancellationState implements State {
    private CoffeeMachine coffeeMachine;
    private float currentMoney;

    public CancellationState(CoffeeMachine coffeeMachine, float currentMoney) {
        this.coffeeMachine = coffeeMachine;
        this.currentMoney = currentMoney;
    }

    @Override
    public void handle() throws InterruptedException {
        System.out.println("You cancel the order");
        System.out.println("Take your " + currentMoney + "! CHURLISH!");
        Thread.sleep(5000);
        coffeeMachine.setState(new NotEnoughMoneyState(coffeeMachine));
    }
}