package patterns.state;

public class MakingCoffeeState implements State {
    private CoffeeMachine coffeeMachine;

    public MakingCoffeeState(CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
    }

    @Override
    public void handle() throws InterruptedException {
        System.out.println("Putting coffee in the cup");
        Thread.sleep(5000);
        System.out.println("Putting boiling water");
        Thread.sleep(5000);
        System.out.println("Putting milk and sugar");
        Thread.sleep(5000);
        System.out.println("Have a good coffee break! Bye!");
        Thread.sleep(5000);
        coffeeMachine.setGramsOfCoffee(coffeeMachine.getCoffeePrice() - 5);
        if (coffeeMachine.getGramsOfCoffee() < 5) {
            coffeeMachine.setState(new NoCoffeeState(coffeeMachine));
        } else {
            coffeeMachine.setState(new NotEnoughMoneyState(coffeeMachine));
        }
    }
}