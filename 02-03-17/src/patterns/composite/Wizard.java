package patterns.composite;

public class Wizard implements Unit {
    @Override
    public String getInformation() {
        return "Wizard";
    }
}