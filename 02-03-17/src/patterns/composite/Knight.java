package patterns.composite;

public class Knight implements Unit {
    @Override
    public String getInformation() {
        return "Knight";
    }
}