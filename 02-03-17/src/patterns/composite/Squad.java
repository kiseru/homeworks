package patterns.composite;

import java.util.Iterator;
import java.util.LinkedList;

public class Squad implements Unit {
    private LinkedList<Unit> units;

    public Squad() {
        this.units = new LinkedList<>();
    }

    @Override
    public String getInformation() {
        String information = "The squad:";
        for (Unit unit : this.units) {
            information += " " + unit.getInformation();
        }

        return information;
    }

    public void add(Unit unit) {
        this.units.add(unit);
        System.out.println(unit.getInformation() + " join the squad");
    }

    public void remove(Unit unit) {
        if (this.units.remove(unit)) {
            System.out.println(unit.getInformation() + " leave the squad");
        } else {
            System.out.println(unit.getInformation() + " is not founded in the squad");
        }
    }
}