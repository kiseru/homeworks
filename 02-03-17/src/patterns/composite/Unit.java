package patterns.composite;

public interface Unit {
    String getInformation();
}