package patterns.composite;

public class Archer implements Unit {
    @Override
    public String getInformation() {
        return "Archer";
    }
}