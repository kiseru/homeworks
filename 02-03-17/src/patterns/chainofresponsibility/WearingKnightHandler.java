package patterns.chainofresponsibility;

public class WearingKnightHandler extends Handler {
    @Override
    public void handle() {
        System.out.println("Knight is being wore");
    }
}