package patterns.chainofresponsibility;

public class EmptyHandler extends Handler {
    @Override
    public void handle() {}
}