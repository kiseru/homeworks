package patterns.chainofresponsibility;

public abstract class Handler {
    protected Handler next;

    public Handler setNext(Handler handler) {
        handler.next = this;
        return handler;
    }

    public abstract void handle();
}
