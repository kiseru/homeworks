package patterns.chainofresponsibility;

public class TakingWeaponHandler extends Handler {
    @Override
    public void handle() {
        System.out.println("Knight takes his weapon");
    }
}