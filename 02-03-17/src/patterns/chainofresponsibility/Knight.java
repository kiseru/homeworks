package patterns.chainofresponsibility;

public class Knight {
    private Handler handler;
    private int countOfHandlers;

    public Knight() {
        this.handler = new EmptyHandler();
        countOfHandlers = 0;
    }

    public void addHandler(Handler handler) {
        this.handler = this.handler.setNext(handler);
        countOfHandlers++;
    }

    public void handleKnight() {
        Handler currentHandler = this.handler;
        for (int i = 0; i < countOfHandlers; i++) {
            currentHandler.handle();
            currentHandler = currentHandler.next;
        }
    }
}