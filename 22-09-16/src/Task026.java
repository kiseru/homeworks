import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task026
 */

public class Task026 {
    private static final double EPS = 0.000000001;

    public static void main(String[] args) {
        double x = new Scanner(System.in).nextDouble();
        double result = 0;
        int n = 1;
        double temp;
        int fact = 1;
        do {
            fact *= n;
            temp = pow(x - 1, n) / (pow(3, n) * (pow(n, 2) + 3) * fact);
            result += temp;
            n++;
        } while (temp > EPS);
        System.out.println(result);
    }

    private static double pow(double a, int n) {
        if (n == 0) return 1;
        return a * pow(a, n - 1);
    }

}
