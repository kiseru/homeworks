import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task030
 */

public class Task030 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        for (int i = 0; i < n; i++) {
            int temp = sc.nextInt();
            int length = lengthOfNumber(temp);
            if ((length == 3 || length == 5) && (length == foo(temp))) count++;
        }
        if (count == 2) System.out.println("yes");
        else System.out.println("no");
    }

    private static int lengthOfNumber(int number) {
        int length = 0;
        while (number != 0) {
            length++;
            number /= 10;
        }
        return length;
    }

    private static int foo(int number) {
        int count = 0;
        while (number != 0) {
            int digit = number % 10;
            if (digit % 2 == 0) count++;
            else count--;
            number /= 10;
        }
        return (abs(count));
    }

    private static int abs(int number) {
        if (number < 0) return -number;
        return number;
    }
}
