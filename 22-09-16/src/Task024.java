import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task024
 */

public class Task024 {
    private static final double EPS = 0.000000001;

    public static void main(String[] args) {
        double x = new Scanner(System.in).nextDouble();
        double result = 0;
        int n = 1;
        double temp;
        do {
            temp = 1 / (n * pow(9, n) * pow(x - 1, 2 * n));
            result += temp;
            n++;
        } while (temp > EPS);
        System.out.println(result);
    }

    private static double pow(double a, int n) {
        if (n == 0) return 1;
        return a * pow(a, n - 1);
    }
}
