/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task025
 */

public class Task025 {
    private static final double EPS = 0.000000001;

    public static void main(String[] args) {
        double result = 0;
        int n = 1;
        double temp;
        do {
            temp = (double)pow(-1, n + 1) / (double)(pow(n, 2) + 3 * n);
            result += temp;
            n++;
        } while (abs(temp) > EPS);
        System.out.println(result);
    }

    private static int pow(int a, int n) {
        if (n == 0) return 1;
        return a * pow(a, n - 1);
    }

    private static double abs(double x) {
        if (x < 0) return -x;
        return x;
    }
}
