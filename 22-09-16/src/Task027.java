import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task027
 */

public class Task027 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        boolean result = false;
        for (int i = 0; i < n; i++) {
            int input = sc.nextInt();
            if (input % 6 == 0 || input % 30 == 0) result = true;
        }
        System.out.println(result);
    }
}
