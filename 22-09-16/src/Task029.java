import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task029
 */

public class Task029 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int n = sc.nextInt();
        int result = 0;
        int i = 0;
        while (n != 0) {
            result += n % 10 * pow(k, i);
            n /= 10;
            i++;
        }
        System.out.println(result);
    }

    private static int pow(int a, int n) {
        if (n == 0) return 1;
        else return a * pow(a, n - 1);
    }
}
