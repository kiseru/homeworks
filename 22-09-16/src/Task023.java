/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task023
 */

public class Task023 {
    private static final double EPS = 0.000000001;

    public static void main(String[] args) {
        double a = 0;
        double temp;
        int n = 1;
        do {
            temp = (double)(2 * n + 3) / (5 * pow(n, 4) + 1);
            a += temp;
            n++;
        } while (temp > EPS);
        System.out.print(a);
    }

    private static double abs(double x) {
        if (x < 0) return -x;
        return x;
    }

    private static int pow(int a, int n) {
        if (n == 0) return 1;
        return a * pow(a, n - 1);
    }
}
