import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task031
 */

public class Task031 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        int mul = 1;
        boolean bool = true;
        for (int i = 1; i <= n; i++) {
            int temp = sc.nextInt();
            mul *= temp;
            if (bool) {
                if (i % 3 == 0 && temp % 3 != 0) {
                    bool = false;
                    continue;
                }
                sum += temp;
            }
        }

        if (bool) System.out.println(sum);
        else System.out.println(mul);
    }
}
