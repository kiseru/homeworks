public class StackArray implements StackInterface {
	private int[] array;
	private int top;
	private int size;

	public StackArray() {
		array = new int[1];
		top = 0;
		size = 0;
	}

	public void push(int value) {
		array[top] = value;
		int[] temp = array;
		array = new int[temp.length + 1];
		for (int i = 0; i < temp.length; i++) {
			array[i] = temp[i];
		}
		size++;
		top++;
	}

	public int pop() throws Exception {
		if (getSize() == 0) throw new Exception("Stack is empty");
		int[] temp = array;
		array = new int[temp.length - 1];
		for (int i = 0; i < array.length; i++) {
			array[i] = temp[i];
		}
		size--;
		top--;
		return array[top];
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		if (getSize() == 0) return false;
		return true;
	}
}