public class QueueArray implements QueueInterface {
	private int[] array;
	private int size;

	public QueueArray() {
		array = new int[1];
		size = 0;
	}

	public void push(int value) {
		int[] temp = array;
		array = new int[temp.length + 1];
		for (int i = 0; i < temp.length; i++) {
			array[i] = temp[i];
		}
		array[size] = value;
		size++;
	}

	public int pop() throws Exception {
		if (isEmpty()) throw new Exception("Queue is empty");
		int[] temp = array;
		array = new int[temp.length - 1];
		for (int i = 0; i < array.length; i++) {
			array[i] = temp[i + 1];
		}
		return temp[0];
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		if (getSize() == 0) return true;
		return false;
	}
}