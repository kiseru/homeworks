public interface StackInterface {
    void push(int value);
    int pop() throws Exception;
    int getSize();
    boolean isEmpty();
}