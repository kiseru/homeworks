public class Queue implements QueueInterface {
	private Node head;
	private Node tail;
	private int size;

	private static class Node {
		private Node next;
		private int value;

		public void setNext(Node _next) {
			next = _next;
		}

		public void setValue(int _value ) {
			value = _value;
		}

		public Node getNext() {
			return next;
		}

		public int getValue() {
			return value;
		}
	}

	public Queue() {
		head = new Node();
		tail = new Node();
		tail.setNext(head);
		size = 0;
	}

	public void push(int value) {
		head.setValue(value);
		Node temp = head;
		head = new Node();
		temp.setNext(head);
		size++;
	}

	public int pop() throws Exception {
		if (isEmpty()) throw new Exception("Queue is empty");
		tail = tail.getNext();
		size--;
		return tail.getValue();
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		if (getSize() == 0) return true;
		return false;
	}
}