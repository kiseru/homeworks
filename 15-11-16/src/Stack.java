public class Stack implements StackInterface {
    private Node top;
    private int size;

    private static class Node {
        private Node next;
        private int value;

        public void setNext(Node _next) {
            next = _next;
        }

        public Node getNext() {
            return  next;
        }

        public void setValue(int _value) {
            value = _value;
        }

        public int getValue() {
            return value;
        }
    }

    public Stack() {
        top = new Node();
        size = 0;
    }

    public void push(int value) {
        top.setValue(value);
        Node temp = top;
        top = new Node();
        top.setNext(temp);
        size++;
    }

    public int pop() {
        if (isEmpty()) throw new Exception("The stack is empty");
        top = top.getNext();
        size--;
        return top.getValue();
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        if (getSize() == 0) return true;
        else return false;
    }
}