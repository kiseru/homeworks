public class Deck implements DeckInterface {
	private Node head;
	private Node tail;
	private int size;

	private static class Node {
		private Node next;
		private Node prev;
		private int value;

		public Node() {}

		public Node(Node _next) {
			next = _next;
		}

		public void setNext(Node _next) {
			next = _next;
		}

		public void setPrev(Node _prev) {
			prev = _prev;
		}

		public void setValue(int _value) {
			value = _value;
		}

		public Node getNext() {
			return next;
		}

		public Node getPrev() {
			return prev;
		}

		public int getValue() {
			return value;
		}
	}

	public Deck() {
		size = 0;
		Node head = new Node();
		Node tail = new Node();
		tail.setNext(head);
		head.setPrev(tail);
	}

	public void pushForward(int value) {
		head.setValue(value);
		head.setNext(new Node());
		head.getNext().setPrev(head);
		head = head.getNext();
		size++;
	}

	public void pushBack(int value) {
		tail.setValue(value);
		tail.setPrev(new Node());
		tail.getPrev().setNext(tail);
		tail = tail.getPrev();
		size++;
	}

	public int popForward() throws Exception {
		if (isEmpty()) throw new Exception("The Deck size is 0");
		head = head.getPrev();
		size--;
		return head.getValue();
	}

	public int popBack() throws Exception {
		if (isEmpty()) throw new Exception("The Deck size is 0");
		tail = tail.getNext();
		size--;
		return tail.getValue();
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		if (getSize() == 0) return true;
		return false;
	}
}