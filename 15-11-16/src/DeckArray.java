public class DeckArray implements DeckInterface {
	private int[] array;
	private int size;

	public DeckArray() {
		size = 0;
		array = new int[1];
	}

	public void pushForward(int value) {
		int[] temp = array;
		array = new int[temp.length + 1];
		array[0] = value;
		for (int i = 0; i < temp.length; i++) {
			array[i + 1] = temp[i];
		}
		size++;
	}

	public void pushBack(int value) {
		int[] temp = array;
		array = new int[temp.length + 1];
		for (int i = 0; i < temp.length; i++) {
			array[i] = temp[i];
		}
		size++;
		array[size] = value;
	}

	public int popForward() throws Exception {
		if (isEmpty()) throw new Exception("Deck is empty");
		int[] temp = array;
		array = new int[temp.length - 1];
		for (int i = 0; i < array.length; i++) {
			array[i] = temp[i + 1];
		}
		size--;
		return temp[0];
	}

	public int popBack() throws Exception {
		if (isEmpty()) throw new Exception("Deck is empty");
		int[] temp = array;
		array = new int[temp.length - 1];
		for (int i = 0; i < array.length; i++) {
			array[i] = temp[i];
		}
		size--;
		return temp[size + 1];
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		if (getSize() == 0) return true;
		return false;
	}
}