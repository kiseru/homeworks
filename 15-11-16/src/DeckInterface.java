public interface DeckInterface {
	void pushForward(int value);
	void pushBack(int value);
	int popForward() throws Exception;
	int popBack() throws Exception;
	int getSize();
	boolean isEmpty();
}
