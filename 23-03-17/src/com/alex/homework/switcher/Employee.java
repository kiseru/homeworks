package com.alex.homework.switcher;

public class Employee {
    private static int count = 0;
    private final int id = count++;

    private Welfare welfare;
    private Behavior behavior;
    private Education education;

    public Employee(Welfare welfare, Behavior behavior, Education education) {
        this.welfare = welfare;
        this.behavior = behavior;
        this.education = education;
    }

    public Welfare getWelfare() {
        return welfare;
    }

    public Behavior getBehavior() {
        return behavior;
    }

    public Education getEducation() {
        return education;
    }

    @Override
    public String toString() {
        return "" + id + " " + welfare + " " + behavior + " " + education;
    }
}
