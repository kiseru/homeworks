package com.alex.homework.switcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Switcher {
    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee(Welfare.RICH, Behavior.HAPPY, Education.COMPETENT));
        employeeList.add(new Employee(Welfare.RICH, Behavior.HAPPY, Education.SILLY));
        employeeList.add(new Employee(Welfare.RICH, Behavior.SAD, Education.COMPETENT));
        employeeList.add(new Employee(Welfare.RICH, Behavior.SAD, Education.SILLY));
        employeeList.add(new Employee(Welfare.POOR, Behavior.HAPPY, Education.COMPETENT));
        employeeList.add(new Employee(Welfare.POOR, Behavior.HAPPY, Education.SILLY));
        employeeList.add(new Employee(Welfare.POOR, Behavior.SAD, Education.COMPETENT));
        employeeList.add(new Employee(Welfare.POOR, Behavior.SAD, Education.SILLY));

        System.out.println(employeeList.stream()
                .filter(switcher(Characteristics.BEHAVIOR))
                .filter(switcher(Characteristics.WELFARE))
                .collect(Collectors.toList()));

        System.out.println(employeeList.stream()
                .filter(switcher(Characteristics.EDUCATION))
                .filter(switcher(Characteristics.WELFARE))
                .collect(Collectors.toList()));

        System.out.println(employeeList.stream()
                .filter(switcher(Characteristics.BEHAVIOR).negate())
                .filter(switcher(Characteristics.EDUCATION))
                .collect(Collectors.toList()));


        System.out.println(Arrays.toString(employeeList.stream()
                .filter(switcher(Characteristics.BEHAVIOR))
                .filter(switcher(Characteristics.WELFARE))
                .toArray(String[]::new)));

        System.out.println(Arrays.toString(employeeList.stream()
                .filter(switcher(Characteristics.EDUCATION))
                .filter(switcher(Characteristics.WELFARE))
                .toArray(String[]::new)));

        System.out.println(Arrays.toString(employeeList.stream()
                .filter(switcher(Characteristics.BEHAVIOR).negate())
                .filter(switcher(Characteristics.EDUCATION))
                .toArray(String[]::new)));
    }

    private static Predicate<Employee> switcher(Characteristics characteristic) {
        Predicate<Employee> isRich = employee -> employee.getWelfare() == Welfare.RICH;
        Predicate<Employee> isHappy = employee -> employee.getBehavior() == Behavior.HAPPY;
        Predicate<Employee> isCompetent = employee -> employee.getEducation() == Education.COMPETENT;
        Predicate<Employee> all = employee -> true;

        switch (characteristic) {
            case WELFARE:
                return isRich;
            case BEHAVIOR:
                return isHappy;
            case EDUCATION:
                return isCompetent;
            default:
                return all;
        }
    }
}
