package com.alex.homework.workingwithtext;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WorkingWithText {
    public static void main(String[] args) {
        System.out.println(changeWords(Arrays.asList("Hi", "My", "name", "is", "Alex", "Bender")));
    }

    private static List<String> changeWords(List<String> words) {
        return words.stream()
                .map(item -> item.length() < 4 ? item.toUpperCase() : item)
                .filter(item -> item.length() < 4 || Pattern.matches("[A-Za-z]*[Ee][A-Za-z]*", item))
                .filter(item -> item.length() < 4 || Pattern.matches("[A-Za-z]*[Bb][A-Za-z]*", item))
                .collect(Collectors.toList());
    }
}
