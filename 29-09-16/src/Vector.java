/**
 * @author Alexandr Kiselev
 *          11-602
 *          Vector for Task038
 */

public class Vector {
    private int[] vector;

    private Vector() {}

    public Vector(int n) {
        vector = new int[n];
    }

    public Vector(Vector vector) {
        this(vector.vector);
    }

    public Vector(int[] vector) {
        this.vector = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            this.vector[i] = vector[i];
        }
    }

    public void set(int value, int index) {
        vector[index] = value;
    }

    public int get(int index) {
        return vector[index];
    }

    public int getDimension() {
        return vector.length;
    }

    public void reduce() {
        int gcd = 1;
        for (int i = 0; i < getDimension(); i++) {
            if (get(i) != 0) {
                gcd = get(i);
                break;
            }
        }

        for (int i = 1; i < vector.length; i++) {
            int a = gcd;
            int b = Math.abs(vector[i]);
            while (a != 0 && b != 0) {
                if (a > b) a %= b;
                else b %= a;
            }
            gcd = a + b;
        }
        for (int i = 0; i < vector.length; i++) {
            vector[i] /= gcd;
        }
    }

    public void add(Vector vector) throws Exception {
        if (getDimension() != vector.getDimension()) new Exception("different dimension");
        for (int i = 0; i < getDimension(); i++) {
            set(get(i) + vector.get(i), i);
        }
    }

    public void multNumber(int ratio) {
        for (int i = 0; i < getDimension(); i++) {
            set(get(i) * ratio, i);
        }
    }
}
