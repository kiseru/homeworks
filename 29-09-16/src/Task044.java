import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task044
 */

public class Task044 {
    public static void main(String[] args) {
        String line = new Scanner(System.in).nextLine();
        String[] words = line.split(" ");
        for (String word : words) {
            if (isCapitalised(word)) System.out.println(word);
        }
    }

    private static boolean isCapitalised(String word) {
        boolean result = true;
        if (word.charAt(0) >= 'a') result = false;
        for (int i = 1; i < word.length() && result; i++) {
            if (word.charAt(i) < 'a') result = false;
        }
        return result;
    }
}
