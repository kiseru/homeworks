import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task045
 */

public class Task046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstLine = scanner.nextLine();
        String secondLine = scanner.nextLine();
        int n = firstLine.length();
        boolean isFirstTheFirst = true;
        if (n > secondLine.length()) {
            n = secondLine.length();
            isFirstTheFirst = false;
        }

        for (int i = 0; i < n; i++) {
            if (firstLine.charAt(i) == secondLine.charAt(i)) continue;
            if (firstLine.charAt(i) < secondLine.charAt(i)) {
                isFirstTheFirst = true;
                break;
            } else {
                isFirstTheFirst = false;
                break;
            }
        }

        if (isFirstTheFirst) System.out.println("The First");
        else System.out.println("The Second");
    }
}
