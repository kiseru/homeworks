import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task045
 */

public class Task045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Team[] teams = new Team[n];
        for (int i = 0; i < n; i++) {
            teams[i] = new Team(scanner.next());
        }
        int k = scanner.nextInt();
        for (int i = 0; i < k; i++) {
            int indexOfFirstTeam = -1;
            int indexOfSecondTeam = -1;
            String firstTeam = scanner.next();
            String secondTeam = scanner.next();
            for (int j = 0; j < n; j++) {
                if (teams[j].name.equals(firstTeam)) indexOfFirstTeam = j;
                if (teams[j].name.equals(secondTeam)) indexOfSecondTeam = j;
            }
            String[] count = scanner.next().split(":");
            int difference = Integer.parseInt(count[0]) - Integer.parseInt(count[1]);
            teams[indexOfFirstTeam].difference += difference;
            teams[indexOfSecondTeam].difference -= difference;
        }

        for (Team team : teams) {
            System.out.printf("%s %d%n", team.name, team.difference);
        }
    }
}

class Team {
    String name;
    int difference;

    Team(String name) {
        this.name = name;
        difference = 0;
    }
}
