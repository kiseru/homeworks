import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task039
 */

public class Task039 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите размерность матрицы: ");
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        System.out.print("Введите размерность массива: ");
        int p = scanner.nextInt();
        String[] array = new String[p];
        int[][] matrix = new int[m][n];
        System.out.println("Введите массив:");
        for (int i = 0; i < p; i++) {
            array[i] = scanner.next();
        }
        System.out.println("Введите матрицу:");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < array.length; i++) {
            Point[] points = search(matrix, array[i]);
        }
    }

    private static Point[] search(int[][] matrix, String str) {
        Point[] points = new Point[str.length()];
        for (int i = 0; i < matrix.length; i++) {
            for ( int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == str.charAt(0)) {
                    points[0] = new Point(j, i);

                }
            }
        }
        return points;
    }
}
