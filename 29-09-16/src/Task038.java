import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task038
 */

public class Task038 {
    public static void main(String[] args) throws Exception {
        Vector[] matrix = readMatrix();
        conversion(matrix);
        printMatrix(matrix);
    }

    private static void conversion(Vector[] matrix) throws Exception {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i + 1; j < matrix[i].getDimension(); j++) {
                int k = matrix[j].get(i);
                matrix[j].multNumber(matrix[i].get(i));
                Vector vector = new Vector(matrix[i]);
                vector.multNumber(k);
                vector.multNumber(-1);
                matrix[j].add(vector);
                matrix[j].reduce();
            }
        }
    }

    private static Vector[] readMatrix() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Vector[] matrix = new Vector[n];
        for (int i = 0; i < n; i++) {
            int[] vector = new int[n];
            for (int j = 0; j < n; j++) {
                vector[j] = scanner.nextInt();
            }
            matrix[i] = new Vector(vector);
        }
        return matrix;
    }

    private static void printMatrix(Vector[] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].getDimension(); j++) {
                System.out.printf("%d ", matrix[i].get(j));
            }
            System.out.println();
        }
    }
}
