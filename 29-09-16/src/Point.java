/**
 * @author Alexandr Kiselev
 *          11-602
 *          Point for Task039
 */

public class Point {
    private int x;
    private int y;

    public Point() {
        this(0, 0);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isNear(Point point) {
        if (Math.abs(x - point.x) == 1 || Math.abs(y - point.y) == 1) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return x + ":" + y;
    }

    @Override
    public Point clone() {
        return new Point(getX(), getY());
    }
}
