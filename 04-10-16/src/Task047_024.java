import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task047_024
 */

public class Task047_024 {
    private static final double EPS = 0.000000001;

    public static void main(String[] args) {
        double x = new Scanner(System.in).nextDouble();
        System.out.println(getResult(x, EPS));
    }

    private static double getResult(double x, double e) {
        double result = 0;
        int n = 1;
        double temp;
        do {
            temp = getSummand(x, n);
            result += temp;
            n++;
        } while (temp > EPS);
        return result;
    }

    private static double getSummand(double x, int n) {
        return 1 / (n * pow(9, n) * pow(x - 1, 2 * n));
    }

    private static double pow(double a, int n) {
        if (n == 0) return 1;
        return a * pow(a, n - 1);
    }
}

