import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task047_008
 */

public class Task047_008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        printAnswer(k);
    }

    private static String getCase(int i, int k) {
        return "" + i + " * " + k + " = " + i * k;
    }

    private static void printAnswer(int k) {
        for (int i = 1; i < 10; i++) {
            System.out.println(getCase(i, k));
        }
    }
}
