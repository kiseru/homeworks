/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task048
 */

public class Task048 {
    public static void main(String[] args) {
        Student student = new Student("Киселёв Александр");
        Professor professor = new Professor("Хасьянов Айрат Фаридович", "Прога");
        professor.evaluate(student);
    }
}
