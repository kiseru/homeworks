/**
 * @author Alexandr Kiselev
 *          11-602
 *          RationalFraction for Task050
 */

public class RationalFraction {
    private int numerator;
    private int denominator;

    public RationalFraction() {
        numerator = 0;
        denominator = 1;
    }

    public RationalFraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void reduce() {
        int a = Math.abs(numerator);
        int b = Math.abs(denominator);
        while (a != 0 && b != 0) {
            if (a > b) a %= b;
            else b %= a;
        }
        int gcd = a + b;
        numerator /= gcd;
        denominator /= gcd;
    }

    public RationalFraction add(RationalFraction rF) {
        RationalFraction result = new RationalFraction(numerator * rF.denominator + rF.numerator * denominator,
                denominator * rF.denominator);
        result.reduce();
        return result;
    }

    public void add2(RationalFraction rF) {
        numerator *= rF.denominator;
        numerator += rF.numerator * denominator;
        denominator *= rF.denominator;
        this.reduce();
    }

    public RationalFraction sub(RationalFraction rF) {
        RationalFraction result = new RationalFraction(numerator * rF.denominator - rF.numerator * denominator,
                denominator * rF.denominator);
        result.reduce();
        return result;
    }

    public void sub2(RationalFraction rF) {
        numerator *= rF.numerator;
        numerator -= rF.numerator * rF.denominator;
        denominator *= rF.denominator;
        this.reduce();
    }

    public RationalFraction mult(RationalFraction rF) {
        RationalFraction result = new RationalFraction(numerator * rF.numerator, denominator * rF.denominator);
        result.reduce();
        return result;
    }

    public void mult2(RationalFraction rF) {
        numerator *= rF.numerator;
        denominator *= rF.denominator;
        this.reduce();
    }

    public RationalFraction div(RationalFraction rF) {
        RationalFraction result = new RationalFraction(numerator * rF.denominator, denominator * rF.numerator);
        result.reduce();
        return result;
    }

    public void div2(RationalFraction rF) {
        numerator *= rF.denominator;
        denominator *= rF.numerator;
        this.reduce();
    }

    @Override
    public String toString() {
        return "" + numerator + "/" + denominator;
    }

    public double value() {
        return numerator / denominator;
    }

    public boolean equals(RationalFraction rF) {
        if (numerator == rF.numerator && denominator == rF.denominator)
            return true;
        return false;
    }

    public int numberPart() {
        return (int)value();
    }
}
