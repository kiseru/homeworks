/**
 * @author Alexandr Kiselev
 *          11-602
 *          Vector2D for Task049
 */

public class Vector2D {
    private double x;
    private double y;

    public Vector2D() {
        x = 0;
        y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Vector2D add(Vector2D vector) {
        return new Vector2D(x + vector.getX(), y + vector.getY());
    }

    public void add2(Vector2D vector) {
        x += vector.getX();
        y += vector.getY();
    }

    public Vector2D sub(Vector2D vector) {
        return new Vector2D(x - vector.getX(), y - vector.getY());
    }

    public void sub2(Vector2D vector) {
        x -= vector.getX();
        y -= vector.getY();
    }

    public Vector2D mult(double k) {
        return new Vector2D(x * k, y * k);
    }

    public void mult2(double k) {
        x *= k;
        y *= k;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double length() {
        return Math.sqrt(x * x + y * y);
    }

    double scalarProduct(Vector2D vector) {
        return x * vector.getX() + y * vector.getY();
    }

    public double cos(Vector2D vector) {
        return scalarProduct(vector) / length() / vector.length();
    }

    public boolean equals(Vector2D vector) {
        if (x == vector.getX() && y == vector.getY())
            return true;
        return false;
    }
}
