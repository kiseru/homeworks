import java.util.Random;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Professor for Task048
 */

public class Professor {
    String name;
    String subject;

    public Professor(String name, String subject) {
        this.name = name;
        this.subject = subject;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public String getSubject() {
        return subject;
    }

    public void evaluate(Student student) {
        Random random = new Random();
        String mark = "";
        switch ((int)(random.nextDouble() * 4 + 2)) {
            case 2:
                mark = "неудовлетворительно";
                break;
            case 3:
                mark = "удовлетворительно";
                break;
            case 4:
                mark = "хорошо";
                break;
            case 5:
                mark = "отлично";
                break;
        }
        System.out.printf("преподаватель %s оценил студента с именем %s по предмету %s на оценку %s.",
                name, student.getName(), subject, mark);
    }
}
