package com.alex.homework.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/mult/*")
public class MultServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getRequestURI();
        String[] arr = path.split("/");
        try {
            int firstValue = Integer.parseInt(arr[2]);
            int secondValue = Integer.parseInt(arr[3]);
            int resultValue = firstValue + secondValue;
            req.setAttribute("firstValue", firstValue);
            req.setAttribute("secondValue", secondValue);
            req.setAttribute("resultValue", resultValue);
            req.getRequestDispatcher("/mult.ftl")
                    .forward(req, resp);
        } catch (NumberFormatException e) {
            req.getRequestDispatcher("/not_found.ftl")
                    .forward(req, resp);
        }
    }
}
