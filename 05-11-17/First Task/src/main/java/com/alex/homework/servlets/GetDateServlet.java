package com.alex.homework.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/getDate")
public class GetDateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String currentDate = new Date().toString();
        req.setAttribute("currentDate", currentDate);
        req.getRequestDispatcher("get-date.ftl")
                .forward(req, resp);
    }
}
