package com.alex.homework.heapsort;

import java.util.Arrays;

public class HeapSort {
    private static int[] heap;
    private static int[] sortedItems;
    private static int cursor = 0;
    private static int sizeOfHeap;

    public static void main(String[] args) {
        int[] array = {3, 6, 4, 7, 1, 2, 5, 9, 8};
        sort(array);
        System.out.println(Arrays.toString(array));

        System.out.println(isHeap(new int[] { 8, 7, 1, 6, 1, 2, 4, 3 }));
    }

    private static void createHeap(int[] array) {
        for (int i = 0; i < array.length; i++) {
            heap[i + 1] = array[i];
            addBalance(i + 1);
        }
    }

    private static void addBalance(int index) {
        if (index >= heap.length || index == 1 || index == 0) return;

        int parent = index / 2;
        if (heap[parent] < heap[index]) {
            int temp = heap[parent];
            heap[parent] = heap[index];
            heap[index] = temp;
            addBalance(parent);
        }
    }

    private static void removeElement() {
        sortedItems[cursor] = heap[1];
        heap[1] = heap[sizeOfHeap - 1];
        sizeOfHeap--;
        removeBalance();
        cursor++;
    }

    private static void removeBalance() {
        for (int i = sizeOfHeap / 2; i > 0; i--) {
            if (2 * i < sizeOfHeap && heap[i] < heap[2 * i]) {
                int temp = heap[i];
                heap[i] = heap[2 * i];
                heap[2 * i] = temp;
            }

            if (2 * i + 1 < sizeOfHeap && heap[i] < heap[2 * i + 1]) {
                int temp = heap[i];
                heap[i] = heap[2 * i + 1];
                heap[2 * i + 1] = temp;
            }
        }
    }

    public static void sort(int[] array) {
        heap = new int[array.length + 1];
        sortedItems = new int[array.length];
        sizeOfHeap = array.length + 1;

        createHeap(array);
        for (int i = 0; i < heap.length - 1; i++) {
            removeElement();
        }

        System.arraycopy(sortedItems, 0, array, 0, array.length);
    }

    public static boolean isHeap(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            if (array[i] < array[2 * (i + 1) - 2]) return false;
            if (array[i] < array[2 * (i + 1) - 1]) return false;
        }
        return true;
    }
}
