package com.alex.structures.stack;

public interface IStack<T> {
    boolean isEmpty();
    T peek();
    IStack push(T t);
    IStack pop();
}
