package com.alex.structures.stack;

public class Stack<T> implements IStack<T> {
    private IStack _stack;
    private T _a;

    public Stack(IStack stack, T a) {
        _stack = stack;
        _a = a;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public T peek() {
        return _a;
    }

    @Override
    public IStack push(T a) {
        return new Stack<T>(this, a);
    }

    @Override
    public IStack pop() {
        return _stack;
    }
}
