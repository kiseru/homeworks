import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 * 11-602
 * 018
 */

public class Task018 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        int n = sc.nextInt();
        double[] a = new double[n + 1];
        for (double i : a) {
            i = sc.nextDouble();
        }
        double result = 0;
        for (int i = n; i >= 0; i--) {
            result += a[i] * pow(x, i);
        }
        System.out.println(result);
    }

    public static double pow(double x, int n) {
        double result = 1;
        for (int i = 0; i < n; i++) {
            result *= x;
        }
        return result;
    }
}
