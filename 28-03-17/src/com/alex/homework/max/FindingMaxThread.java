package com.alex.homework.max;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FindingMaxThread extends Thread {
    private final int[] array;
    private final int from;
    private final int to;
    private static int max;

    public FindingMaxThread(int[] array, int from, int to) {
        this.array = array;
        this.from = from;
        this.to = to;
        max = this.array[0];
    }

    @Override
    public void run() {
        for (int i = from; i < to && i < array.length; i++) {
            Lock lock = new ReentrantLock();
            lock.lock();
            try {
                max = max < array[i] ? array[i] : max;
            } finally {
                lock.unlock();
            }
        }
    }

    public static int getMax() {
        return max;
    }
}