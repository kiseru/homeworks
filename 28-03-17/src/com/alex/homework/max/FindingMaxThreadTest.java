package com.alex.homework.max;

import java.util.ArrayList;

public class FindingMaxThreadTest {
    public static void main(String[] args) {
        int[] array = { 1, 0, 2, 5, 4, 3, 9, 7, 8 };

        int step = array.length % 5 == 0 ? array.length / 5 : array.length / 4;
        ArrayList<FindingMaxThread> threads = new ArrayList<>();
        for (int i = 0; i < array.length; i += step) {
            threads.add(new FindingMaxThread(array, i, i + step));
        }

        threads.forEach(thread -> {
            thread.start();

            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        });

        System.out.println(FindingMaxThread.getMax());
    }
}
