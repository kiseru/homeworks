package com.alex.server;

import java.io.File;

public class ServerTest {
    public static void main(String[] args) {
        Server server = new Server(".\\src");

        server.start();

        while(server.isAlive());

        File[] files = server.getFiles();
        for (File file : files) {
            System.out.println(file.getName());
        }
    }
}
