package com.alex.server;

import java.io.File;
import java.util.Arrays;

public class Server extends Thread {
    private String pathName;
    private File[] files;

    public Server(String pathName) {
        this.pathName = pathName;
    }

    @Override
    public void run() {
        File directory = new File(pathName);
        if (!directory.isDirectory()) {
            System.out.println(pathName + " is not directory");
            return;
        }

        files = Arrays.stream(directory.list())
                .map(fileName -> new File(pathName, fileName))
                .filter(File::isFile)
                .toArray(File[]::new);
    }

    public File[] getFiles() {
        return files;
    }
}
