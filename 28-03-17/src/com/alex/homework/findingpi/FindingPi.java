package com.alex.homework.findingpi;

public class FindingPi {
    public static void main(String[] args) {
        CountServer server = new CountServer();
        server.start();
        try {
            server.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(CountServer.getPI());
    }
}
