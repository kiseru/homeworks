package com.alex.homework.findingpi;

public class FindTruePoint extends Thread {
    private double x;
    private double y;

    public FindTruePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void run() {
        if (x * x + y * y <= 1) {
            CountServer.isTrue();
        } else {
            CountServer.isFalse();
        }
    }
}
