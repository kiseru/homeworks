package com.alex.homework.findingpi;

import java.util.Random;

public class CountServer extends Thread {
    private static int count = 0;
    private static int countOfTrue = 0;

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            double x = random.nextDouble();
            double y = random.nextDouble();
            FindTruePoint thread = new FindTruePoint(x, y);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static synchronized void isTrue() {
        count++;
        countOfTrue++;
    }

    public static synchronized void isFalse() {
        count++;
    }

    public static double getPI() {
        return (double)countOfTrue / count * 4;
    }
}