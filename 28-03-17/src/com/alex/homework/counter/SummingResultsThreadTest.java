package com.alex.homework.counter;

public class SummingResultsThreadTest {
    public static void main(String[] args) {
        SummingResultsThread thread = new SummingResultsThread();
        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(thread.getSum());
    }
}
