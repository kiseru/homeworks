package com.alex.homework.counter;

public class IteratorThread extends Thread {
    private int iterator;

    public IteratorThread() {
        iterator = 0;
    }

    @Override
    public void run() {
        while (iterator < 10) {
            iterator++;
        }
    }

    public int getIterator() {
        return iterator;
    }
}
