package com.alex.homework.counter;

import java.util.LinkedList;
import java.util.List;

public class SummingResultsThread extends Thread {
    private int sum;

    public SummingResultsThread() {
        sum = 0;
    }

    @Override
    public void run() {
        List<IteratorThread> threadList = new LinkedList<>();

        for (int i = 0; i < 10; i++) {
            IteratorThread thread = new IteratorThread();
            threadList.add(thread);
            thread.start();

            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

        for (IteratorThread thread : threadList) {
            sum += thread.getIterator();
        }
    }

    public int getSum() {
        return sum;
    }
}
