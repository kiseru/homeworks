package com.alex.homework.maxcountdividers;

public class CountDividersThread extends Thread {
    private int number;

    CountDividersThread(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        int countOfDividers = 0;
        int sqrtNumber = (int) Math.sqrt(number);
        for (int i = 1; i <= sqrtNumber; i++) {
            countOfDividers += number % i == 0 ? 2 : 0;
        }

        countOfDividers -= sqrtNumber == number / sqrtNumber ? 1 : 0;

        MaxCountDividersThread.setMax(countOfDividers, number);
    }
}
