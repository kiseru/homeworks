package com.alex.homework.maxcountdividers;

public class MaxCountDividersThreadTest {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        System.out.println("Starting counting");

        MaxCountDividersThread maxCountDividersThread = new MaxCountDividersThread(1, 1000000);
        maxCountDividersThread.start();

        try {
            maxCountDividersThread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        System.out.printf("Число %d имеет наибольшее количество делителей.\n",
                MaxCountDividersThread.getNumber());
        System.out.printf("А именно: %d\n", MaxCountDividersThread.getMaxCountDividers());
        long finish = System.currentTimeMillis();
        System.out.println(finish - start);
    }
}