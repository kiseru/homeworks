package com.alex.homework.maxcountdividers;

public class MaxCountDividersThread extends Thread {
    private static int maxCountDividers = 0;
    private static int number = 0;
    private int from;
    private int to;

    public MaxCountDividersThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            Thread thread = new CountDividersThread(i);
            thread.start();

            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static synchronized void setMax(int max, int newNumber) {
        if (max > maxCountDividers) {
            maxCountDividers = max;
            number = newNumber;
        }
    }

    public static int getMaxCountDividers() {
        return maxCountDividers;
    }

    public static int getNumber() {
        return number;
    }
}