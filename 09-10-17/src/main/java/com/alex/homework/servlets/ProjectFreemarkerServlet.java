package com.alex.homework.servlets;


import freemarker.ext.servlet.FreemarkerServlet;

import javax.servlet.annotation.WebServlet;

@WebServlet(name = "freemarker_servlet",
        urlPatterns = "*.ftl")
public class ProjectFreemarkerServlet extends FreemarkerServlet {}

