package com.alex.homework.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/result")
public class ResultServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String result;

        if ((result = req.getParameter("number_of_characters")) != null) {
            getView(req, resp, "/number_of_characters.ftl", result);
        } else if ((result = req.getParameter("number_of_words")) != null) {
            getView(req, resp, "/number_of_words.ftl", result);
        } else if ((result = req.getParameter("number_of_proposals")) != null) {
            getView(req, resp, "/number_of_proposals.ftl", result);
        } else if ((result = req.getParameter("number_of_paragraphs")) != null) {
            getView(req, resp, "/number_of_paragraphs.ftl", result);
        } else {
            resp.sendRedirect("/process");
        }
    }

    private void getView(HttpServletRequest req, HttpServletResponse resp, String viewUrl, String result) throws ServletException, IOException {
        req.setAttribute("result", result);
        req.getRequestDispatcher(viewUrl)
                .forward(req, resp);
    }
}
