package com.alex.homework.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/process")
public class ProcessServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/process.ftl")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String text = req.getParameter("text");
        String operation = req.getParameter("operation");

        if (operation.equals("s")) {
            resp.sendRedirect("result?number_of_characters=" + text.length());
        } else if (operation.equals("w")) {
            resp.sendRedirect("result?number_of_words=" + text.split(" ").length);
        } else if (operation.equals("p")) {
            resp.sendRedirect("result?number_of_proposals=" + text.split("[.!?]").length);
        } else if (operation.equals("a")) {
            resp.sendRedirect("result?number_of_paragraphs=" + text.split("\\n").length);
        }
    }
}
