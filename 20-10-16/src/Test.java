import com.math.*;

/**
 * @author Alexandr Kiselev
 *         11-602
 *         Test
 */

public class Test {
    public static void main(String[] args) throws Exception {
        RationalFraction rF1 = new RationalFraction(2, 4);
        RationalFraction rF2 = new RationalFraction(3, 7);
        RationalComplexNumber num = new RationalComplexNumber(rF1, rF2);
        System.out.println(num);
    }
}
