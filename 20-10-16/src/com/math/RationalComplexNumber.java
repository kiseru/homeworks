package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task057
 */

public class RationalComplexNumber {
    private RationalFraction real;
    private RationalFraction image;

    public RationalComplexNumber() throws Exception {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalComplexNumber(RationalFraction real, RationalFraction imaginary) {
        this.real = real;
        this.image = imaginary;
    }

    public void setReal(RationalFraction real) {
        this.real = real;
    }

    public void setImage(RationalFraction image) {
        this.image = image;
    }

    public RationalFraction getReal() {
        return real;
    }

    public RationalFraction getImage() {
        return image;
    }

    public void reduce() {
        real.reduce();
        image.reduce();
    }

    public RationalComplexNumber add(RationalComplexNumber number) throws Exception {
        RationalComplexNumber result = new RationalComplexNumber(real, image);
        result.real.add2(number.real);
        result.image.add2(number.image);
        result.reduce();
        return result;
    }

    public void add2(RationalComplexNumber number) {
        real.add2(number.real);
        image.add2(number.image);
        reduce();
    }

    public RationalComplexNumber sub(RationalComplexNumber number) throws Exception {
        RationalComplexNumber result = new RationalComplexNumber(real, image);
        result.real.sub2(number.real);
        result.image.sub2(number.image);
        result.reduce();
        return new RationalComplexNumber(real.sub(number.real), image.sub(image));
    }

    public RationalComplexNumber mult(RationalComplexNumber number) throws Exception {
        RationalComplexNumber result = new RationalComplexNumber(real.mult(number.real), image.mult(number.real));
        result.real.sub2(image.mult(number.image));
        result.image.add2(real.mult(number.image));
        result.reduce();
        return result;
    }

    @Override
    public String toString() {
        reduce();
        RationalFraction temp = null;
        try {
            temp = new RationalFraction(Math.abs(real.getNumerator()), real.getDenominator());
        } catch (Exception ex) {}
        String result = "" + image + " * i ";
        if (real.getNumerator() < 0) result += "- " + temp;
        else result += "+ " + real;
        return result;
    }

    @Override
    public RationalComplexNumber clone() {
        return new RationalComplexNumber(real, image);
    }
}
