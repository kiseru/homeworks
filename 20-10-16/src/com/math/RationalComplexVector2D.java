package com.math;

public class RationalComplexVector2D {
    RationalComplexNumber[] vector;

    public RationalComplexVector2D() throws Exception {
        this(new RationalComplexNumber(), new RationalComplexNumber());
    }

    public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y) {
        vector = new RationalComplexNumber[2];
        vector[0] = x;
        vector[1] = y;
    }

    public void setX(RationalComplexNumber x) {
        vector[0] = x;
    }

    public void setY(RationalComplexNumber y) {
        vector[1] = y;
    }

    public void set(RationalComplexNumber value, int index) {
        vector[index] = value;
    }

    public RationalComplexNumber getX() {
        return vector[0];
    }

    public RationalComplexNumber getY() {
        return vector[1];
    }

    public RationalComplexNumber get(int index) {
        return vector[index];
    }

    public RationalComplexVector2D add(RationalComplexVector2D vector) throws Exception {
        RationalComplexVector2D result = new RationalComplexVector2D(getX(), getY());
        result.vector[0].add(vector.getY());
        result.vector[1].add(vector.getY());
        result.vector[0].reduce();
        result.vector[1].reduce();
        return result;
    }

    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }

    public RationalComplexNumber scalarProduct(RationalComplexVector2D vector) throws Exception {
        return getX().mult(vector.getX()).add(getY().mult(vector.getY()));
    }
}
