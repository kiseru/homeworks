package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task054
 */

public class ComplexVector2D {
    ComplexNumber[] vector;

    public ComplexVector2D() {
        this(new ComplexNumber(), new ComplexNumber());
    }

    public ComplexVector2D(ComplexNumber x, ComplexNumber y) {
        vector = new ComplexNumber[2];
        setX(x);
        setY(y);
    }

    public void setX(ComplexNumber x) {
        vector[0] = x;
    }

    public void setY(ComplexNumber y) {
        vector[1] = y;
    }

    void set(ComplexNumber value, int index) {
        vector[index] = value;
    }

    public ComplexNumber getX() {
        return vector[0];
    }

    public ComplexNumber getY() {
        return vector[1];
    }

    ComplexNumber get(int i) {
        return vector[i];
    }

    public ComplexVector2D add(ComplexVector2D vector) {
        return new ComplexVector2D(getX().add(vector.getX()), getY().add(vector.getY()));
    }

    @Override
    public String toString() {
        return "(" + getX().toString() + ", " + getY().toString() + ")";
    }

    public ComplexNumber scalarProduct(ComplexVector2D vector) {
        ComplexNumber sum = new ComplexNumber();
        for (int i = 0; i < 2; i++) {
            sum.add2(get(i).mult(vector.get(i)));
        }
        return sum;
    }

    public boolean equals(ComplexVector2D vector) {
        if (getX().equals(vector.getX()) && getY().equals(vector.getY()))
            return true;
        return false;
    }
}
