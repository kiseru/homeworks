package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          ComplexNumber for Task051
 */

public class ComplexNumber {
    private double real;
    private double image;

    public ComplexNumber() {
        this(0, 0);
    }

    public ComplexNumber(double real, double imaginary) {
        this.real = real;
        this.image = imaginary;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public void setImage(double image) {
        this.image = image;
    }

    public double getReal() {
        return real;
    }

    public double getImage() {
        return image;
    }

    public ComplexNumber add(ComplexNumber number) {
        return new ComplexNumber(real + number.real, image + number.image);
    }

    public void add2(ComplexNumber number) {
        real += number.real;
        image += number.image;
    }

    public ComplexNumber sub(ComplexNumber number) {
        return new ComplexNumber(real - number.real, image - number.image);
    }

    public void sub2(ComplexNumber number) {
        real -= number.real;
        image -= number.image;
    }

    public ComplexNumber multNumber(double k) {
        return new ComplexNumber(real * k, image * k);
    }

    public void multNumber2(double k) {
        real *= k;
        image *= k;
    }

    public ComplexNumber mult(ComplexNumber number) {
        return new ComplexNumber(real * number.real - image * number.image,
                image * number.real + real * number.image);
    }

    public void mult2(ComplexNumber number) {
        real = real * number.real - image * number.image;
        image = image * number.real + real * number.image;
    }

    public ComplexNumber div(ComplexNumber number) {
        double real = (this.real * number.real + this.image * number.image);
        real /= number.real * number.real + number.image * number.image;
        double imaginary = (this.image * number.real - this.real * number.image);
        imaginary /= number.real * number.real + number.image * number.image;
        return new ComplexNumber(real, imaginary);
    }

    public void div2(ComplexNumber number) {
        real = (this.real * number.real + this.image * number.image) /
                (number.real * number.real + number.image * number.image);
        image = (this.image * number.real - this.real * number.image) /
                (number.real * number.real + number.image * number.image);
    }

    public double length() {
        return real * real + image * image;
    }

    @Override
    public String toString() {
        String result = "" + image + " * i ";
        if (real < 0) result += "- " + Math.abs(real);
        else result += "+ " + real;
        return result;
    }

    public double arg() {
        if (real > 0)
            return Math.atan(image / real);
        else if (real < 0 && image >= 0)
            return Math.atan(image / real) + Math.PI;
        else if (real < 0 && image < 0)
            return Math.atan(image / real) - Math.PI;
        else if (real == 0 && image > 0)
            return Math.PI / 2;
        else if (real == 0 && image < 0)
            return -Math.PI / 2;
        else
            return 0.0 / 0;
    }

    public ComplexNumber pow(double n) {
        if (n == 0)
            return new ComplexNumber(1, 1);
        return pow(n - 1).mult(this);
    }

    public boolean equals(ComplexNumber number) {
        if (real == number.real && image == number.image) return true;
        return false;
    }

    @Override
    public ComplexNumber clone() {
        return new ComplexNumber(getReal(), getImage());
    }
}
