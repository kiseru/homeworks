package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          RationalMatrix2x2 for Task055
 */

public class RationalMatrix2x2 {
    private RationalFraction[][] matrix;

    public RationalMatrix2x2() throws Exception {
        this(new RationalFraction());
    }

    public RationalMatrix2x2(RationalFraction rF) throws Exception {
        this(rF.clone(), rF.clone(), rF.clone(), rF.clone());
    }

    public RationalMatrix2x2(RationalFraction rF1, RationalFraction rF2, RationalFraction rF3, RationalFraction rF4) {
        matrix = new RationalFraction[2][2];
        matrix[0][0] = rF1;
        matrix[0][1] = rF2;
        matrix[1][0] = rF3;
        matrix[1][1] = rF4;
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 matrix) throws Exception {
        RationalMatrix2x2 result = new RationalMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result.matrix[i][j].add2(this.matrix[i][j].add(matrix.matrix[i][j]));
            }
        }
        return result;
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 matrix) throws Exception {
        RationalMatrix2x2 result = new RationalMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    result.matrix[i][j].add(this.matrix[i][k].mult(matrix.matrix[k][j]));
                }
            }
        }
        return result;
    }

    private RationalFraction get(int i, int j) {
        return matrix[i][j];
    }

    public RationalFraction det() throws Exception {
        return matrix[0][0].mult(matrix[1][1]).sub(matrix[0][1].mult(matrix[1][0]));
    }

    public RationalVector2D multVector(RationalVector2D vector) throws Exception {
        RationalVector2D result = new RationalVector2D();
        for (int i = 0; i < 2; i++) {
            for (int k = 0; k < 2; k++) {
                result.set(result.get(i).add(get(i, k).mult(vector.get(k))), i);
            }
        }
        return result;
    }
}
