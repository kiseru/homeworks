package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task059
 */

public class RationalComplexMatrix2x2 {
    RationalComplexNumber[][] matrix;

    public RationalComplexMatrix2x2() throws Exception {
        this(new RationalComplexNumber());
    }

    public RationalComplexMatrix2x2(RationalComplexNumber number) {
        this(number.clone(), number.clone(), number.clone(), number.clone());
    }

    public RationalComplexMatrix2x2(RationalComplexNumber first, RationalComplexNumber second,
                            RationalComplexNumber third, RationalComplexNumber forth) {
        matrix = new RationalComplexNumber[2][2];
        matrix[0][0] = first;
        matrix[0][1] = second;
        matrix[1][0] = third;
        matrix[1][1] = forth;
    }

    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 matrix) throws Exception {
        RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result.matrix[i][j].add2(this.matrix[i][j].add(matrix.matrix[i][j]));
            }
        }
        return result;
    }

    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 matrix) throws Exception {
        RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    result.matrix[i][j].add2(this.matrix[i][k].mult(matrix.matrix[k][j]));
                }
            }
        }
        return result;
    }

    public RationalComplexNumber det() throws Exception {
        return matrix[0][0].mult(matrix[0][1]).sub(matrix[1][0].mult(matrix[1][1]));
    }

    public RationalComplexVector2D multVector(RationalComplexVector2D vector) throws Exception {
        RationalComplexVector2D result = new RationalComplexVector2D();
        for (int i = 0; i < 2; i++) {
            for (int k = 0; k < 2; k++) {
                result.set(result.get(i).add(this.matrix[i][k].mult(vector.get(k))), i);
            }
        }
        return result;
    }
}
