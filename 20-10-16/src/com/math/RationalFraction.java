package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          RationalFraction for Task053
 */

public class RationalFraction {
    private int numerator;
    private int denominator;

    public RationalFraction() throws Exception {
        this(0, 1);
    }

    public RationalFraction(int numerator, int denominator) throws Exception {
        setNumerator(numerator);
        setDenominator(denominator);
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) throws Exception {
        if (denominator == 0) throw new Exception("Нельзя делить на ноль");
        else this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void reduce() {
        int a = Math.abs(numerator);
        int b = Math.abs(denominator);
        while (a != 0 && b != 0) {
            if (a > b) a %= b;
            else b %= a;
        }
        int gcd = a + b;
        numerator /= gcd;
        denominator /= gcd;
        if (denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }
    }

    public RationalFraction add(RationalFraction rF) throws Exception {
        RationalFraction result = new RationalFraction(numerator * rF.denominator + rF.numerator * denominator,
                denominator * rF.denominator);
        result.reduce();
        return result;
    }

    public void add2(RationalFraction rF) {
        numerator *= rF.denominator;
        numerator += rF.numerator * denominator;
        denominator *= rF.denominator;
        this.reduce();
    }

    public RationalFraction sub(RationalFraction rF) throws Exception {
        RationalFraction result = new RationalFraction(numerator * rF.denominator - rF.numerator * denominator,
                denominator * rF.denominator);
        result.reduce();
        return result;
    }

    public void sub2(RationalFraction rF) {
        numerator *= rF.numerator;
        numerator -= rF.numerator * rF.denominator;
        denominator *= rF.denominator;
        this.reduce();
    }

    public RationalFraction mult(RationalFraction rF) throws Exception {
        RationalFraction result = new RationalFraction(numerator * rF.numerator, denominator * rF.denominator);
        result.reduce();
        return result;
    }

    public void mult2(RationalFraction rF) {
        numerator *= rF.numerator;
        denominator *= rF.denominator;
        this.reduce();
    }

    public RationalFraction div(RationalFraction rF) throws Exception {
        RationalFraction result = new RationalFraction(numerator * rF.denominator, denominator * rF.numerator);
        result.reduce();
        return result;
    }

    public void div2(RationalFraction rF) {
        numerator *= rF.denominator;
        denominator *= rF.numerator;
        this.reduce();
    }

    @Override
    public String toString() {
        return "" + numerator + "/" + denominator;
    }

    public double value() {
        return numerator / denominator;
    }

    public boolean equals(RationalFraction rF) {
        if (numerator == rF.numerator && denominator == rF.denominator)
            return true;
        return false;
    }

    public int numberPart() {
        return (int)value();
    }

    @Override
    public RationalFraction clone() {
        try {
            return new RationalFraction(getNumerator(), getDenominator());
        } catch(Exception ex) {
            return null;
        }
    }
}
