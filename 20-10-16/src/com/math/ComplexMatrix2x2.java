package com.math;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task056
 */

public class ComplexMatrix2x2 {
    private ComplexNumber[][] matrix;

    public ComplexMatrix2x2() {
        this(new ComplexNumber());
    }

    public ComplexMatrix2x2(ComplexNumber number) {
        this(number.clone(), number.clone(), number.clone(), number.clone());
    }

    public ComplexMatrix2x2(ComplexNumber firstNumber, ComplexNumber secondNumber,
                            ComplexNumber thirdNumber, ComplexNumber forthNumber) {
        matrix = new ComplexNumber[2][2];
        matrix[0][0] = firstNumber;
        matrix[0][1] = secondNumber;
        matrix[1][0] = thirdNumber;
        matrix[1][1] = forthNumber;
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 matrix) {
        ComplexMatrix2x2 result = new ComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result.matrix[i][j].add2(this.matrix[i][j].add(matrix.matrix[i][j]));
            }
        }
        return result;
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 matrix) {
        ComplexMatrix2x2 result = new ComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    result.matrix[i][j].add(this.matrix[i][k].mult(matrix.matrix[k][j]));
                }
            }
        }
        return result;
    }

    private ComplexNumber get(int i, int j) {
        return matrix[i][j];
    }

    public ComplexNumber det() throws Exception {
        return matrix[0][0].mult(matrix[1][1]).sub(matrix[0][1].mult(matrix[1][0]));
    }

    public ComplexVector2D multVector(ComplexVector2D vector) throws Exception {
        ComplexVector2D result = new ComplexVector2D();
        for (int i = 0; i < 2; i++) {
            for (int k = 0; k < 2; k++) {
                result.set(result.get(i).add(get(i, k).mult(vector.get(k))), i);
            }
        }
        return result;
    }
}
