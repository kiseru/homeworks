package com.alex.homework.task2;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws IOException {
        Random random = new Random();

        List<Integer> randomNumbers = new LinkedList<>();
        for (int i = 0; i < 1000000; i++) {
            randomNumbers.add(random.nextInt(1000000));
        }

        randomNumbers.stream().filter(item -> item > 0 && item < 13)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .forEach((key, value) -> System.out.println(key + ": " + value))
    }
}
