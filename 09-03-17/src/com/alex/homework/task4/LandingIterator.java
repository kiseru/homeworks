package com.alex.homework.task4;

import com.alex.homework.task3.Passenger;

import java.util.Iterator;
import java.util.List;

public class LandingIterator implements Iterator<Passenger> {
    private List<Passenger> passengerList;
    private int pointer;

    public LandingIterator(List<Passenger> passengerList) {
        this.passengerList = passengerList;
        this.pointer = 0;
    }

    @Override
    public boolean hasNext() {
        return pointer < passengerList.size();
    }

    @Override
    public Passenger next() {
        return passengerList.get(pointer);
    }
}
