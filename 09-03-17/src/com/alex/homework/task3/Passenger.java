package com.alex.homework.task3;

public class Passenger implements Comparable<Passenger> {
    private Card card;
    private String name;

    public Passenger(String name, Card card) {
        this.card = card;
        this.name = name;
    }

    @Override
    public int compareTo(Passenger passenger) {
        if (this.card.equals(passenger.card)) {
            return 0;
        } else if (this.card == Card.SIMPLE) {
            return -1;
        } else if (this.card == Card.GOLD) {
            return 1;
        } else if (passenger.card == Card.SIMPLE) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        return name + " " + card;
    }
}
