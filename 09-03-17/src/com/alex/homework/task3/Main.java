package com.alex.homework.task3;

import java.util.PriorityQueue;

public class Main {
    public static void main(String[] args) {
        PriorityQueue<Passenger> passengers = new PriorityQueue<>();

        passengers.add(new Passenger("1", Card.SILVER));
        passengers.add(new Passenger("2", Card.GOLD));
        passengers.add(new Passenger("3", Card.SIMPLE));
        passengers.add(new Passenger("4", Card.SILVER));
        passengers.add(new Passenger("5", Card.GOLD));
        passengers.add(new Passenger("6", Card.SIMPLE));
        passengers.add(new Passenger("7", Card.SILVER));
        passengers.add(new Passenger("8", Card.SIMPLE));
        passengers.add(new Passenger("9", Card.GOLD));

        while (!passengers.isEmpty()) {
            System.out.println(passengers.poll());
        }
    }
}
