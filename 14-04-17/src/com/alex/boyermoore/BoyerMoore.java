package com.alex.boyermoore;

import java.util.HashMap;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BoyerMoore {
    private String sourceText;
    private String pattern;

    private HashMap<Character, Integer> badSymbolShiftTable;
    private int[] goodSymbolShiftTable;
    private String[] patternLetters;
    private int[] suffixes;

    public BoyerMoore(String sourceText, String pattern) {
        this.sourceText = sourceText;
        this.pattern = pattern;
    }

    public boolean run() {
        generateBadSymbolShiftTable();
        generatePatternLetters();
        generateSuffixes();
        generateGoodSymbolShiftTable();

        int pointer = pattern.length() - 1;
        while (pointer < sourceText.length()) {
            int shift = pattern.length();

            int counter = 0;
            int coincidencesCounter = 0;

            for (int i = 0; counter < pattern.length(); i++) {
                int currentPosition = pointer - i;
                if (sourceText.charAt(currentPosition) == pattern.charAt(pattern.length() - i - 1)) {
                    coincidencesCounter++;
                } else if (coincidencesCounter != 0) {
                    int shiftPointer = -1;
                    for (int j = 0; j < suffixes.length; j++) {
                        if (suffixes[j] == coincidencesCounter) {
                            shiftPointer = j;
                        }
                    }

                    shift = shiftPointer == -1 ? pattern.length() - 1 : goodSymbolShiftTable[shiftPointer];
                    break;
                } else {
                    break;
                }

                if (coincidencesCounter == pattern.length()) return true;

                counter++;
            }

            if (coincidencesCounter == 0) {
                shift = badSymbolShiftTable.get(sourceText.charAt(pointer));
            }

            pointer += shift;
        }

        return false;
    }

    private void generateBadSymbolShiftTable() {
        badSymbolShiftTable = new HashMap<>();

        Stream.iterate(0, i -> i + 1).limit(pattern.length() - 1)
                .forEach(i -> badSymbolShiftTable.put(pattern.charAt(i), pattern.length() - 1 - i));

        sourceText.chars()
                .mapToObj(letter -> ((char) letter))
                .filter(letter -> !badSymbolShiftTable.containsKey(letter))
                .forEach(letter -> badSymbolShiftTable.put(letter, pattern.length()));
    }

    private void generatePatternLetters() {
        patternLetters = pattern.chars()
                .mapToObj(letter -> String.valueOf((char) letter))
                .toArray(String[]::new);
    }

    private void generateSuffixes() {
        suffixes = new int[patternLetters.length];
        int pointer = 0;
        while (pointer < patternLetters.length) {
            int endPointer = patternLetters.length - 1;
            int result = 0;
            for (int i = pointer; i >= 0; i--) {
                if (!patternLetters[i].equals(patternLetters[endPointer])) break;
                result++;
                endPointer--;
            }

            suffixes[pointer] = result;
            pointer++;
        }
    }

    private void generateGoodSymbolShiftTable() {
        goodSymbolShiftTable = IntStream.iterate(0, i -> i + 1).limit(pattern.length())
                .map(i -> suffixes[i] != 0 ? pattern.length() - i - 1 : pattern.length() - 1)
                .toArray();

        goodSymbolShiftTable[goodSymbolShiftTable.length - 1] = 1;
    }
}
