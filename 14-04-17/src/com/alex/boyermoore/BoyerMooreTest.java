package com.alex.boyermoore;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoyerMooreTest {
    @Test
    void testCreatingBadSymbolsShiftTable() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        BoyerMoore boyerMoore = new BoyerMoore("GCATCGCAGAGAGTATACAGTACG", "GCAGAGAG");

        Method boyerMooreGenerateBadSymbolShiftTableMethod = BoyerMoore.class.getDeclaredMethod("generateBadSymbolShiftTable");
        boyerMooreGenerateBadSymbolShiftTableMethod.setAccessible(true);
        boyerMooreGenerateBadSymbolShiftTableMethod.invoke(boyerMoore);

        Field boyerMooreBadSymbolShiftTableField = BoyerMoore.class.getDeclaredField("badSymbolShiftTable");
        boyerMooreBadSymbolShiftTableField.setAccessible(true);
        HashMap<Character, Integer> badSymbolShiftTable = (HashMap<Character, Integer>) boyerMooreBadSymbolShiftTableField.get(boyerMoore);

        char[] symbols = new char[] { 'A', 'C', 'G', 'T' };
        int[] indexes = new int[] { 1, 6, 2, 8 };

        boolean isEverythingCorrect = IntStream.iterate(0, i -> i + 1).limit(symbols.length)
                .mapToObj(i -> badSymbolShiftTable.containsKey(symbols[i]) && badSymbolShiftTable.get(symbols[i]).equals(indexes[i]))
                .reduce(true, (acc, value) -> acc = acc && value);

        assertTrue(isEverythingCorrect);
    }

    @Test
    void testGeneratePatternSymbols() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        BoyerMoore boyerMoore = new BoyerMoore("GCATCGCAGAGAGTATACAGTACG", "GCAGAGAG");

        Method boyerMooreGeneratePatternLettersMethod = BoyerMoore.class.getDeclaredMethod("generatePatternLetters");
        boyerMooreGeneratePatternLettersMethod.setAccessible(true);
        boyerMooreGeneratePatternLettersMethod.invoke(boyerMoore);

        Field boyerMoorePatternLettersField = BoyerMoore.class.getDeclaredField("patternLetters");
        boyerMoorePatternLettersField.setAccessible(true);
        String[] patternLetters = (String[]) boyerMoorePatternLettersField.get(boyerMoore);

        String[] testingValues = new String[] { "G", "C", "A", "G", "A", "G", "A", "G" };

        boolean test = false;
        if (patternLetters.length == testingValues.length) {
            test = IntStream.iterate(0, i -> i + 1).limit(testingValues.length)
                    .mapToObj(i -> patternLetters[i].equals(testingValues[i]))
                    .reduce(true, (acc, value) -> acc = acc && value);
        }

        assertTrue(test);
    }

    @Test
    void testGenerateSuffixes() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        BoyerMoore boyerMoore = new BoyerMoore("GCATCGCAGAGAGTATACAGTACG", "GCAGAGAG");

        Method boyerMooreGeneratePatternLettersMethod = BoyerMoore.class.getDeclaredMethod("generatePatternLetters");
        boyerMooreGeneratePatternLettersMethod.setAccessible(true);
        boyerMooreGeneratePatternLettersMethod.invoke(boyerMoore);

        Method boyerMooreGenerateSuffixesMethod = BoyerMoore.class.getDeclaredMethod("generateSuffixes");
        boyerMooreGenerateSuffixesMethod.setAccessible(true);
        boyerMooreGenerateSuffixesMethod.invoke(boyerMoore);

        Field boyerMooreSuffixesField = BoyerMoore.class.getDeclaredField("suffixes");
        boyerMooreSuffixesField.setAccessible(true);
        int[] suffixes = (int[]) boyerMooreSuffixesField.get(boyerMoore);

        int[] testingValues = new int[] { 1, 0, 0, 2, 0, 4, 0, 8 };

        boolean test = false;
        if (suffixes.length == testingValues.length) {
            test = IntStream.iterate(0, i -> i + 1).limit(suffixes.length)
                    .mapToObj(i -> suffixes[i] == testingValues[i])
                    .reduce(true, (acc, value) -> acc = acc && value);
        }

        assertTrue(test);
    }

    @Test
    void testGenerateGoodSymbolShiftTable() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        BoyerMoore boyerMoore = new BoyerMoore("GCATCGCAGAGAGTATACAGTACG", "GCAGAGAG");

        Method boyerMooreGeneratePatternLettersMethod = BoyerMoore.class.getDeclaredMethod("generatePatternLetters");
        boyerMooreGeneratePatternLettersMethod.setAccessible(true);
        boyerMooreGeneratePatternLettersMethod.invoke(boyerMoore);

        Method boyerMooreGenerateSuffixesMethod = BoyerMoore.class.getDeclaredMethod("generateSuffixes");
        boyerMooreGenerateSuffixesMethod.setAccessible(true);
        boyerMooreGenerateSuffixesMethod.invoke(boyerMoore);

        Method boyerMooreGenerateGoodSymbolShiftTableMethod = BoyerMoore.class.getDeclaredMethod("generateGoodSymbolShiftTable");
        boyerMooreGenerateGoodSymbolShiftTableMethod.setAccessible(true);
        boyerMooreGenerateGoodSymbolShiftTableMethod.invoke(boyerMoore);

        Field boyerMooreGoodSymbolShiftTableField = BoyerMoore.class.getDeclaredField("goodSymbolShiftTable");
        boyerMooreGoodSymbolShiftTableField.setAccessible(true);
        int[] goodSymbolShiftTable = (int[]) boyerMooreGoodSymbolShiftTableField.get(boyerMoore);

        int[] testingValues = new int[] { 7, 7, 7, 4, 7, 2, 7, 1 };

        boolean test = false;
        if (goodSymbolShiftTable.length == testingValues.length) {
            test = IntStream.iterate(0, i -> i + 1).limit(goodSymbolShiftTable.length)
                    .mapToObj(i -> goodSymbolShiftTable[i] == testingValues[i])
                    .reduce(true, (acc, value) -> acc = acc && value);
        }

        assertTrue(test);
    }

    @Test
    void findingFirst() {
        assertTrue(new BoyerMoore("GCATCGCAGAGAGTATACAGTACG", "GCAGAGAG").run());
    }

    @Test
    void findingSecond() {
        assertTrue(new BoyerMoore("BESS_KNEW_ABOUT_BAOBABS", "BAOBAB").run());
    }

    @Test
    void findingThird() {
        assertTrue(new BoyerMoore("BESS_KNEW_ABOUT_BAOBABS", "BAOBABS").run());
    }

    @Test
    void findingFourth() {
        assertFalse(new BoyerMoore("BESS_KNEW_ABOUT_BAOBABS", "KNOW").run());
    }
}