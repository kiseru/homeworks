package com.alex.structures.queue;

public interface IQueue<T> {
    boolean isEmpty();
    void insert(T value);
    T remove();
}
