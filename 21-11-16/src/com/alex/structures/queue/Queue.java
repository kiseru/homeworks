package com.alex.structures.queue;

public class Queue<T> implements IQueue<T> {
	private Node<T> left;
	private Node<T> right;
	private int size;

	public static class Node<T> {
		Node<T> left;
		T value;
	}

	public Queue() {
		size = 0;
	}

	@Override
	public boolean isEmpty() {
		if (size == 0) return true;
		return false;
	}

	@Override
	public void insert(T value) {
		size++;
		left.value = value;
		Node<T> temp = left;
		left = new Node<T>();
		temp.left = left;
	}

	@Override
	public T remove() {
		if (isEmpty()) throw new RuntimeException("Queue is empty");
		right = right.left;
		return right.value;
	}
}