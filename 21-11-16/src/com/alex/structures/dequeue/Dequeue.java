package com.alex.structures.dequeue;

public class Dequeue<T> implements IDequeue<T> {
    private Node<T> left;
    private Node<T> right;
    private int size;

    private static class Node<T> {
        T value;
        Node<T> left;
        Node<T> right;
    }

    public Dequeue() {
        left = new Node<T>();
        right = new Node<T>();
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) return true;
        return false;
    }

    @Override
    public void insertLeft(T value) {
        left.value = value;
        left.right = new Node<T>();
        left = left.right;
    }

    @Override
    public T removeLeft() {
        if (size == 0) {
            size--;
            left = left.left;
            return left.value;
        } else {
            throw new RuntimeException("Dequeue is empty");
        }
    }

    @Override
    public void insertRight(T value) {
        right.value = value;
        right.left = new Node<T>();
        right = right.left;
    }

    @Override
    public T removeRight() {
        if (size == 0) {
            size--;
            right = right.right;
            return right.value;
        } else {
            throw new RuntimeException("Dequeue is empty");
        }
    }
}