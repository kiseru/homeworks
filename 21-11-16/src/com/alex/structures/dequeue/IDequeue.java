package com.alex.structures.dequeue;

public interface IDequeue<T> {
    boolean isEmpty();
    void insertLeft(T value);
    T removeLeft();
    void insertRight(T value);
    T removeRight();
}
