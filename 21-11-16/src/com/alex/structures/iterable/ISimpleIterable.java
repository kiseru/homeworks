package com.alex.structures.iterable;

public interface ISimpleIterable<T> {
    ISimpleIterator<T> iterator();
}
