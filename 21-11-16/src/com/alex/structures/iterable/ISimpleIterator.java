package com.alex.structures.iterable;

public interface ISimpleIterator<T> {
    boolean hasNext();
    T next();
    void remove();
}
