package com.alex.structures.stack;

public class Stack<T> implements IStack<T> {
    private IStack stack;
    private T value;

    public Stack(IStack _stack, T _value) {
        stack = _stack;
        value = _value;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public T peek() {
        return value;
    }

    @Override
    public IStack push(T value) {
        return new Stack<T>(this, value);
    }

    @Override
    public IStack pop() {
        return stack;
    }
}