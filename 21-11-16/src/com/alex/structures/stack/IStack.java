package com.alex.structures.stack;

public interface IStack<T> {
    boolean isEmpty();
    T peek();
    IStack push(T value);
    IStack pop();
}
