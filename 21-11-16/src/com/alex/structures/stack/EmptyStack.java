package com.alex.structures.stack;

import java.util.EmptyStackException;

public class EmptyStack<T> implements IStack<T> {

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public T peek() {
        throw new EmptyStackException();
    }

    @Override
    public IStack push(T value) {
        return new Stack<T>(this, value);
    }

    @Override
    public IStack pop() {
        throw new EmptyStackException();
    }
}
