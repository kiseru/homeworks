package com.alex.structures.set;

import com.alex.structures.set.ISet;

public class SetArray implements ISet {
    Object[] set;
    int size;

    public SetArray() {
        size = 0;
        set = new Object[size];
    }

    @Override
    public void add(Object object) {
        Object[] temp = set;
        set = new Object[size + 1];
        for (int i = 0; i < size; i++) {
            set[i] = temp[i];
        }
        set[size] = object;
        size++;
    }

    @Override
    public void clear() {
        size = 0;
        set = new Object[size];
    }

    @Override
    public boolean contains(Object object) {
        boolean isIn = false;
        for (int i = 0; i < size && !isIn; i++) {
            if (set[i].equals(object)) {
                isIn = true;
            }
        }
        return isIn;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) return true;
        return false;
    }

    @Override
    public void remove(Object object) {
        boolean isIn = false;
        int i = 0;
        for (; i < size && !isIn; i++) {
            if (set[i].equals(object)) isIn = true;
        }

        if (isIn) {
            Object[] temp = set;
            size--;
            set = new Object[size];
            for (int j = 0; j < i; j++) {
                set[i] = temp[i];
            }
            for (int j = i; j < size; j++) {
                set[i] = temp[i + 1];
            }
        }
    }

    @Override
    public int size() {
        return size;
    }
}