package com.alex.structures.set;

import com.alex.structures.set.ISet;

public class Set implements ISet {
    private Node last;
    private int size;

    private static class Node {
        private Node next;
        private Object value;

        public Node() {}

        public Node(Node next) {
            setNext(next);
        }

        public Node(Node next, Object value) {
            setNext(next);
            getNext().setValue(value);
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }

    public Set() {
        last = new Node();
        size = 0;
    }

    @Override
    public void add(Object object) {
        last = new Node(last, object);
        size++;
    }

    @Override
    public void clear() {
        last = new Node();
        size++;
    }

    @Override
    public boolean contains(Object object) {
        boolean isIn = false;
        Node temp = last.getNext();
        for (int i = size; i > 0 && !isIn; i--) {
            if (temp.equals(object)) isIn = true;
        }
        return isIn;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) return true;
        return false;
    }

    @Override
    public void remove(Object object) {
        boolean hasDone = false;
        Node temp = last;
        for (int i = size; i > 0 && !hasDone; i--) {
            if (temp.getNext().getValue().equals(object)) {
                temp.setNext(temp.getNext().getNext());
                hasDone = true;
                size--;
            }
        }
    }

    @Override
    public int size() {
        return size;
    }
}
