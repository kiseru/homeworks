package com.alex.structures.set;

public interface ISet {
    void add(Object object);
    void clear();
    boolean contains(Object object);
    boolean isEmpty();
    void remove(Object object);
    int size();
}