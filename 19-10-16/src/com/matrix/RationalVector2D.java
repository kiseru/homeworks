package com.matrix;

import java.util.concurrent.ExecutionException;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          RationalVector2D for Task053
 */

public class RationalVector2D {
    RationalFraction[] vector;

    public RationalVector2D() throws Exception {
        vector = new RationalFraction[2];
        vector[0] = new RationalFraction();
        vector[1] = new RationalFraction();
    }

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        vector = new RationalFraction[2];
        vector[0] = x;
        vector[1] = y;
    }

    public void setX(RationalFraction x) {
        vector[0] = x;
    }

    public void setY(RationalFraction y) {
        vector[1] = y;
    }

    public void set(RationalFraction value, int index) {
        vector[index] = value;
    }

    public RationalFraction getX() {
        return vector[0];
    }

    public RationalFraction getY() {
        return vector[1];
    }

    public RationalFraction get(int i) {
        return vector[i];
    }

    public RationalVector2D add(RationalVector2D vector) throws Exception {
        RationalVector2D result = new RationalVector2D(getX(), getY());
        result.setX(result.getX().add(vector.getX()));
        result.setY(result.getY().add(vector.getY()));
        result.vector[0].reduce();
        result.vector[1].reduce();
        return result;
    }

    @Override
    public String toString() {
        return "(" + getX().toString() + ", " + getY().toString() + ")";
    }

    public double length() throws Exception {
        return Math.sqrt(getX().mult(getX()).add(getY().mult(getY())).value());
    }

    public RationalFraction scalarProduct(RationalVector2D vector) throws Exception {
        RationalFraction sum = new RationalFraction();
        for (int i = 0; i < 2; i++) {
            sum.add(get(i).mult(vector.get(i)));
        }
        return sum;
    }

    public boolean equals(RationalVector2D vector) {
        if (getX().equals(vector.getX()) && getY().equals(vector.getY()))
            return true;
        return false;
    }
}
