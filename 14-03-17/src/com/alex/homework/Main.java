package com.alex.homework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        List<File> files = getFiles(args);
        Map<String, Integer> score  = getScore(getWords(getTexts(files)));
        System.out.println(score);
    }

    private static List<File> getFiles(String[] args) x{
        return Arrays.stream(args).parallel().map(File::new)
                .sequential().collect(Collectors.toList());
    }

    private static List<String> getTexts(List<File> files) throws FileNotFoundException {
        List<String> texts = new LinkedList<>();
        for (File file : files) {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            texts.addAll(reader.lines().collect(Collectors.toCollection(LinkedList::new)));
        }

        return texts;
    }

    private static List<String> getWords(List<String> texts) {
        List<String> words = new LinkedList<>();
        for (String text : texts) {
            words.addAll(Arrays.stream(text.split(" ")).collect(Collectors.toCollection(LinkedList::new)));
        }

        return words;
    }

    private static Map<String, Integer> getScore(List<String> words) {
        Map<String, Integer> result = new HashMap<>();

        for (String word : words) {
            if (hasLatinLetters(word)) continue;
            if (result.containsKey(word)) {
                result.put(word, result.get(word));
            } else {
                result.put(word, 1);
            }
        }

        return result;
    }

    private static boolean hasLatinLetters(String word) {
        for (int i = 0; i < word.length(); i++) {
            char character = word.charAt(i);
            if (!(character > 'А' && character < 'Я' || character > 'а' && character < 'я')) {
                return true;
            }
        }

        return false;
    }
}
