package com.alex.calculator.views;

public class MainMenu implements Menu {
    private final String text = Line.getLine("Menu") + "\n" +
            "1.Addition\n" +
            "2.Subtraction\n" +
            "3.Multiplication\n" +
            "4.Division\n" +
            "5.Exponentiation\n" +
            "6.Exit\n" +
            Line.getLine();

    private static MainMenu menu;

    private MainMenu() {}

    public static MainMenu getMenu() {
        if (menu == null) {
            synchronized (MainMenu.class) {
                if (menu == null) {
                    menu = new MainMenu();
                }
            }
        }

        return menu;
    }

    public void show() {
        System.out.println(text);
    }
}