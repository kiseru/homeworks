package com.alex.calculator.views;

public class Line {
    private final static int WORD_POSITION = 5;
    private final static int LENGTH = 35;

    private Line() {}

    public static String getLine(String word) {
        String line = word;

        for (int i = 0; i < WORD_POSITION; i++) {
            line = "=" + line;
        }

        for (int i = line.length(); i < LENGTH; i++) {
            line += "=";
        }

        return line;
    }

    public static String getLine() {
        return getLine("") + "\n";
    }
}