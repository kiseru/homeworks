package com.alex.calculator.views;

public class ErrorMenu implements Menu {
    private final String text = Line.getLine("Menu") + "\n" +
            "1.Try again\n" +
            "2.Exit\n" +
            Line.getLine();

    private static ErrorMenu errorMenu;

    private ErrorMenu() {}

    public static ErrorMenu getMenu() {
        if (errorMenu == null) {
            synchronized (ErrorMenu.class) {
                if (errorMenu == null) {
                    errorMenu = new ErrorMenu();
                }
            }
        }

        return errorMenu;
    }

    @Override
    public void show() {
        System.out.println(text);
    }
}