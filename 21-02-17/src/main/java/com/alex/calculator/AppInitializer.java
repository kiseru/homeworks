package com.alex.calculator;

import com.alex.calculator.controllers.Controller;
import com.alex.calculator.controllers.MainController;

public class AppInitializer {
    public static void main(String[] args) {
        System.out.println("Welcome to Alex's calculator!");
        Controller controller = MainController.getController();

        while (controller != null) {
            controller = controller.run();
        }
    }
}