package com.alex.calculator.controllers;

import com.alex.calculator.views.Line;

import java.util.Scanner;

public class SubtractionController implements Controller {
    private static SubtractionController controller;

    private SubtractionController() {}

    public static SubtractionController getController() {
        if (controller == null) {
            synchronized (SubtractionController.class) {
                if (controller == null) {
                    controller = new SubtractionController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(Line.getLine("Subtraction"));
        System.out.print("Input first value: ");
        double firstValue = scanner.nextDouble();

        System.out.print("Input second value: ");
        double secondValue = scanner.nextDouble();

        double result = firstValue - secondValue;
        System.out.println("The answer: " + result);

        System.out.println(Line.getLine());

        return MainController.getController();
    }
}