package com.alex.calculator.controllers;

import com.alex.calculator.views.Line;

import java.util.Scanner;

public class ExponentiationController implements Controller {
    private static ExponentiationController controller;

    private ExponentiationController() {}

    public static ExponentiationController getController() {
        if (controller == null) {
            synchronized (ExponentiationController.class) {
                if (controller == null) {
                    controller = new ExponentiationController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(Line.getLine("Exponentiation"));
        System.out.print("Input value: ");
        double firstValue = scanner.nextDouble();

        System.out.print("Input exponent: ");
        int secondValue = scanner.nextInt();

        double result = binPow(firstValue, secondValue);
        System.out.println("The answer: " + result);

        System.out.println(Line.getLine());

        return MainController.getController();
    }

    private static double binPow(double value, int exponent) {
        double result;

        if (exponent == 0) {
            result = 1;
        } else if (exponent % 2 == 0) {
            double temp = binPow(value, exponent / 2);
            result = temp * temp;
        } else {
            result = binPow(value, exponent - 1) * value;
        }

        return result;
    }
}