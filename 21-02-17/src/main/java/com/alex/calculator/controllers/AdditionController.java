package com.alex.calculator.controllers;

import com.alex.calculator.views.Line;

import java.util.Scanner;

public class AdditionController implements Controller {
    private static AdditionController controller;

    private AdditionController() {}

    public static AdditionController getController() {
        if (controller == null) {
            synchronized (AdditionController.class) {
                if (controller == null) {
                    controller = new AdditionController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(Line.getLine("Addition"));
        System.out.print("Input first value: ");
        double firstValue = scanner.nextDouble();

        System.out.print("Input second value: ");
        double secondValue = scanner.nextDouble();

        double result = firstValue + secondValue;
        System.out.println("The answer: " + result);

        System.out.println(Line.getLine());

        return MainController.getController();
    }
}