package com.alex.calculator.controllers;

import com.alex.calculator.views.Line;

import java.util.Scanner;

public class DivisionController implements Controller {
    private static DivisionController controller;

    private DivisionController() {}

    public static DivisionController getController() {
        if (controller == null) {
            synchronized (DivisionController.class) {
                if (controller == null) {
                    controller = new DivisionController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(Line.getLine("Division"));
        System.out.print("Input first value: ");
        double firstValue = scanner.nextDouble();

        System.out.print("Input second value: ");
        double secondValue = scanner.nextDouble();

        double result = firstValue / secondValue;
        System.out.println("The answer: " + result);

        System.out.println(Line.getLine());

        return MainController.getController();
    }
}