package com.alex.calculator.controllers;

import com.alex.calculator.views.Line;

import java.util.Scanner;

public class MultiplicationController implements Controller {
    private static MultiplicationController controller;

    private MultiplicationController() {}

    public static MultiplicationController getController() {
        if (controller == null) {
            synchronized (MultiplicationController.class) {
                if (controller == null) {
                    controller = new MultiplicationController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(Line.getLine("Multiplication"));
        System.out.print("Input first value: ");
        double firstValue = scanner.nextDouble();

        System.out.print("Input second value: ");
        double secondValue = scanner.nextDouble();

        double result = firstValue * secondValue;
        System.out.println("The answer: " + result);

        System.out.println(Line.getLine());

        return MainController.getController();
    }
}