package com.alex.calculator.controllers;

import com.alex.calculator.views.ErrorMenu;

import java.util.Scanner;

public class ErrorController implements Controller {
    private static ErrorController controller;

    private ErrorController() {}

    public static ErrorController getController() {
        if (controller == null) {
            synchronized (ErrorController.class) {
                if (controller == null) {
                    controller = new ErrorController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        ErrorMenu.getMenu().show();

        System.out.print("Choose item from the list: ");
        int input = new Scanner(System.in).nextInt();
        System.out.println();

        Controller controller;
        try {
            if (input == 1) {
                controller = MainController.getController();
            } else if (input == 2) {
                controller = ExitController.getController();
            } else {
                throw new Exception("Input error!");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + "\n");
            controller = ErrorController.getController();
        }

        return controller;
    }
}