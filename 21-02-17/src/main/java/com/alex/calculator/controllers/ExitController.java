package com.alex.calculator.controllers;

public class ExitController implements Controller {
    private static ExitController controller;

    private ExitController() {}

    public static ExitController getController() {
        if (controller == null) {
            synchronized (ExitController.class) {
                if (controller == null) {
                    controller = new ExitController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        System.out.println("\nThank you for using our APP!");
        return null;
    }
}