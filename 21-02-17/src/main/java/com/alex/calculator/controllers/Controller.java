package com.alex.calculator.controllers;

public interface Controller {
    Controller run();
}