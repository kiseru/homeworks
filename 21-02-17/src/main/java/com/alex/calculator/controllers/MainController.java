package com.alex.calculator.controllers;

import com.alex.calculator.views.MainMenu;

import java.util.Scanner;

public class MainController implements Controller {
    private static MainController controller;

    private MainController() {}

    public static MainController getController() {
        if (controller == null) {
            synchronized (MainController.class) {
                if (controller == null) {
                    controller = new MainController();
                }
            }
        }

        return controller;
    }

    @Override
    public Controller run() {
        MainMenu.getMenu().show();

        System.out.print("Choose item from the list: ");
        int input = new Scanner(System.in).nextInt();
        System.out.println();

        Controller controller;
        try {
            if (input == 1) {
                controller = AdditionController.getController();
            } else if (input == 2) {
                controller = SubtractionController.getController();
            } else if (input == 3) {
                controller = MultiplicationController.getController();
            } else if (input == 4) {
                controller = DivisionController.getController();
            } else if (input == 5) {
                controller = ExponentiationController.getController();
            } else if (input == 6) {
                controller = ExitController.getController();
            } else {
                throw new Exception("Input error!");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + "\n");
            controller = ErrorController.getController();
        }

        return controller;
    }
}