public class Division {
	public static void main(String[] args) {
		double x = 1;
		double y = 2;
		double z = 3;
		double result = x + 2;
		result *= y;
		result -= z;
		result /= y;
		result += y * z;
		System.out.println(result);
	}
}