public class TableOfMultiply {
	public static void main(String[] args) {
		int k = 2;
		for (int i = 1; i < 10; i++) {
			System.out.format("%d * %d = %d%n", i, k, i * k);
		}
	}
}