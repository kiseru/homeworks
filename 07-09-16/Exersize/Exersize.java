public class Exersize {
	public static void main(String[] args) {
		int a = 1;
		int b = 2;
		System.out.format("%d + %d = %d%n", a, b, a + b);
		System.out.format("%d - %d = %d%n", a, b, a - b);
		System.out.format("%d - %d = %d%n", b, a, b - a);
		System.out.format("%d * %d = %d%n", a, b, a * b);
		System.out.format("%d / %d = %f%n", a, b, (float)a / b);
		System.out.format("%d %% %d = %d%n", a, b, a % b);
		System.out.format("%d / %d = %f%n", b, a, (float)b / a);
		System.out.format("%d %% %d = %d%n", b, a, b % a);
	}
}