public class Solution {
	public static void main(String[] args) {
		int x = 1;
		double result = pow(x, 5);
		result += 6 * pow(x, 4);
		result += 10 * pow(x, 3);
		result += 25 * pow(x, 2);
		result += 30 * x;
		result += 101;
		System.out.println(result);
	}

	private static double pow(double a, int n) {
		double result = a;
		for (int i = 0; i < n - 1; i++) {
			result *= a;
		}
		return result;
	}
}