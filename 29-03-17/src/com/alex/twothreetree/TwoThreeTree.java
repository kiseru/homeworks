package com.alex.twothreetree;

public class TwoThreeTree {
    private INode23 root;
    private EmptyTree23 emptyTree;

    public INode23 getEmptyTree23() {
        if (emptyTree == null) {
            emptyTree = new EmptyTree23();
        }

        return emptyTree;
    }

    public TwoThreeTree() {
        root = getEmptyTree23();
    }

    private INode23 findNode(int key) {
        INode23 root = this.root;
        while (!root.testNode(key)) {
            root = root.nextNode(key);
        }

        return root;
    }

    public void insert(int key) {
        findNode(key).insertValue(key);
    }

    @Override
    public String toString() {
        return root.toString();
    }

    private interface INode23 {
        boolean isEmpty();
        boolean testNode(int key);
        INode23 nextNode(int key);
        INode23 insertNode(Node2 node);
        INode23 insertValue(int key);
        INode23 getParent();
        void setParent(INode23 newParent);
        INode23 replaceChild(INode23 oldNode, INode23 newNode);
    }

    abstract class Node23 implements INode23 {
        protected INode23 parent;

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public INode23 getParent() {
            return parent;
        }

        @Override
        public void setParent(INode23 newParent) {
            parent = newParent;
        }
    }

    private class EmptyTree23 extends Node23 {
        private EmptyTree23() {}

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public INode23 insertValue(int key) {
            System.out.println("Empty tree got " + key);
            root = new Node2(key, getEmptyTree23());
            return root;
        }

        @Override
        public INode23 insertNode(Node2 node) {
            root = node;
            node.setParent(getEmptyTree23());
            return node;
        }

        @Override
        public boolean testNode(int key) {
            return true;
        }

        @Override
        public INode23 nextNode(int key) {
            return this;
        }

        @Override
        public INode23 replaceChild(INode23 oldNode, INode23 newNode) {
            return oldNode == root ? root = newNode : root;
        }

        @Override
        public String toString() {
            return "+";
        }
    }

    private  class Leaf23 extends Node23 {
        private Leaf23() {}

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public INode23 insertValue(int key) {
            return insertNode(new Node2(key));
        }

        @Override
        public INode23 insertNode(Node2 node) {//Is this ever called?
            System.out.println("Inserting into leaf: " + node.getValue());
            node.setParent(parent);
            parent.replaceChild(this, node);
            return node;
        }

        @Override
        public boolean testNode(int key) {
            return true;
        }

        @Override
        public INode23 nextNode(int key) {//could we make it smarter
            return this;
        }

        @Override
        public INode23 replaceChild(INode23 oldNode, INode23 newNode) {
            return this;
        }

        @Override
        public String toString() {
            return "-";
        }
    }

    class Node2 extends Node23 {
        INode23 left;
        INode23 right;
        int value;

        public Node2(int key) {
            this(key, null);
        }

        public Node2(int key, INode23 parent) {
            this(key, parent, new Leaf23(), new Leaf23());
        }

        public Node2(int key, INode23 parent, INode23 left, INode23 right) {
            value = key;
            this.parent = parent;
            this.right = right;
            this.left = left;
            System.out.println("Node2 created " + key);
        }

        public void setRight(INode23 right) {
            this.right = right;
        }

        public void setLeft(INode23 left) {
            this.left = left;
        }

        public INode23 getLeft() {
            return left;
        }

        public INode23 getRight() {
            return right;
        }

        public int getValue() {
            return value;
        }

        @Override
        public boolean testNode(int key) {
            System.out.println("Node2.textNode " + key);
            return left.isEmpty() && right.isEmpty() || value == key;
        }

        @Override
        public INode23 nextNode(int key) {
            return key > value ? right : left;
        }

        @Override
        public INode23 insertNode(Node2 node) {
            int key = node.getValue();
            return node.getValue() < value ? insertMin(node, key) : insertMax(node, key);
        }

        @Override
        public INode23 insertValue(int key) {
            return insertNode(new Node2(key));
        }

        private INode23 insertMin(Node2 node, int key) {
            System.out.println("Node.insertMin " + key + " parent = " + getParent());
            Node3 node3 = new Node3(key, value, getParent());
            node3.setLeft(node.getLeft());
            node3.setMiddle(node.getRight());
            node3.setRight(getRight());
            node.getLeft().setParent(node3);
            node.getRight().setParent(node3);
            getRight().setParent(node3);
            return parent.replaceChild(this, node3);
        }

        private INode23 insertMax(Node2 node, int key) {
            System.out.println("Node2.insertMax " + key + " parent = " + getParent());
            Node3 node3 = new Node3(value, key, getParent());
            node3.setLeft(getLeft());
            node3.setMiddle(node.getLeft());
            node3.setRight(node.getRight());
            node.getLeft().setParent(node3);
            node.getRight().setParent(node3);
            getLeft().setParent(node3);
            return parent.replaceChild(this, node3);
        }

        @Override
        public INode23 replaceChild(INode23 oldNode, INode23 newNode) {
            if (left == oldNode) {
                left = newNode;
            } else {
                right = newNode;
            }

            return newNode;
        }

        @Override
        public String toString() {
            return "[" + left + "," + value + "," + right + "]";
        }
    }

    class Node3 extends Node23 {
        INode23 left;
        INode23 middle;
        INode23 right;
        int min;
        int max;

        public Node3(int minKey, int maxKey) {
            this(minKey, maxKey, null);
        }

        public Node3(int minKey, int maxKey, INode23 parent) {
            min = minKey;
            max = maxKey;
            this.parent = parent;
            middle = new Leaf23();
            left = new Leaf23();
            right = new Leaf23();
            System.out.println("Node3 created " + min + "-" + max);
        }

        public void setLeft(INode23 left) {
            this.left = left;
        }

        public void setMiddle(INode23 middle) {
            this.middle = middle;
        }

        public void setRight(INode23 right) {
            this.right = right;
        }

        @Override
        public boolean testNode(int key) {
            return left.isEmpty() && right.isEmpty() && middle.isEmpty() || equalsNodeValue(key);
        }

        private boolean equalsNodeValue(int key) {
            return key == min || key == max;
        }

        @Override
        public INode23 nextNode(int key) {
            return key < min ? left : key < max ? middle : right;
        }

        @Override
        public INode23 insertNode(Node2 node) {
            int key = node.getValue();
            return key < min ? insertLeft(node) : key < max ? insertMiddle(node) : insertRight(node);
        }

        @Override
        public INode23 insertValue(int key) {
            return insertNode(new Node2(key));
        }

        private INode23 insertLeft(Node2 node) {
            System.out.println("Node3.insertLeft " + node.getValue());
            Node2 newRoot = new Node2(min, parent);
            Node2 newLeft = node;
            newLeft.setParent(newRoot);
            Node2 newRight = new Node2(max, newRoot);

            newRight.setLeft(middle);
            newRight.setRight(right);

            newRoot.setLeft(newLeft);
            newRoot.setRight(newRight);

            return parent.insertNode(newRoot);
        }

        private INode23 insertMiddle(Node2 node) {
            System.out.println("Node3.insertRight " + node.getValue());
            Node2 newRoot = node;
            Node2 newLeft = new Node2(min, newRoot);
            Node2 newRight = new Node2(max, newRoot);

            newLeft.setLeft(left);
            newLeft.setRight(node.getLeft());

            newRight.setLeft(node.getRight());
            newRight.setRight(right);

            newRoot.setLeft(newLeft);
            newRight.setRight(newRight);

            return parent.insertNode(newRoot);
        }

        private INode23 insertRight(Node2 node) {
            System.out.println("Node3.insertRight " + node.getValue());

            Node2 newRoot = new Node2(max, parent);
            Node2 newLeft = new Node2(min, newRoot);
            Node2 newRight = node;

            newRight.setParent(newRoot);

            newLeft.setLeft(left);
            newLeft.setRight(middle);

            newRoot.setLeft(newLeft);
            newRoot.setRight(newRight);

            return parent.insertNode(newRoot);
        }

        @Override
        public INode23 replaceChild(INode23 oldNode, INode23 newNode) {
            if (left == oldNode) {
                left = newNode;
            } else {
                if (middle == oldNode) {
                    middle = newNode;
                } else {
                    right = newNode;
                }
            }

            return newNode;
        }

        @Override
        public String toString() {
            return "[" + left + "," + min + "/" + middle + "/" + max + "," + right + "]";
        }
    }
}
