package com.alex.heap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class HeapSort<T extends Comparable<T>> {
    private ArrayList<T> heap;
    private ArrayList<T> sortedItems;
    private int cursor;
    private int sizeOfHeap;

    public static void main(String[] args) {
        int[] array = {3, 6, 4, 7, 1, 2, 5, 9, 8};
        ArrayList<Integer> arrayList = Arrays.stream(array)
                .boxed()
                .collect(Collectors.toCollection(ArrayList::new));
        HeapSort sort = new HeapSort(arrayList);
        arrayList = sort.sort();
        System.out.println(arrayList);
    }

    public HeapSort(ArrayList<T> array) {
        heap = new ArrayList<T>();
        heap.addAll(array);
        heap.add(0, heap.get(0));
        sortedItems = new ArrayList<T>();
        sortedItems.addAll(array);
        sizeOfHeap = heap.size();
        cursor = 0;
    }

    private void createHeap() {
        for (int i = heap.size() / 2; i > 0; i--) {
            if (2 * i < heap.size() && heap.get(i).compareTo(heap.get(2 * i)) < 0) {
                T temp = heap.get(i);
                heap.set(i, heap.get(2 * i));
                heap.set(2 * i, temp);
            }

            if (2 * i + 1 < heap.size() && heap.get(i).compareTo(heap.get(2 * i + 1)) < 0) {
                T temp = heap.get(i);
                heap.set(i, heap.get(2 * i + 1));
                heap.set(2 * i + 1, temp);
            }
        }
    }

    private void removeElement() {
        sortedItems.set(cursor, heap.get(1));
        heap.set(1, heap.get(sizeOfHeap - 1));
        sizeOfHeap--;
        balance();
        cursor++;
    }

    private void balance() {
        for (int i = sizeOfHeap / 2; i > 0; i--) {
            if (2 * i < heap.size() && heap.get(i).compareTo(heap.get(2 * i)) < 0) {
                T temp = heap.get(i);
                heap.set(i, heap.get(2 * i));
                heap.set(2 * i, temp);
            }

            if (2 * i + 1 < heap.size() && heap.get(i).compareTo(heap.get(2 * i + 1)) < 0) {
                T temp = heap.get(i);
                heap.set(i, heap.get(2 * i + 1));
                heap.set(2 * i + 1, temp);
            }
        }
    }

    public ArrayList<T> sort() {
        ArrayList<T> arrayList = new ArrayList<T>(heap.size() - 1);

        createHeap();
        for (int i = 0; i < heap.size() - 1; i++) {
            removeElement();
        }

        arrayList.addAll(sortedItems);

        return arrayList;
    }
}
