import java.util.LinkedList;
import java.util.Random;
import java.util.regex.Pattern;

public class FirstTask {

    public static void main(String[] args) {

        Random random = new Random();
        LinkedList<String> tenNumbers = new LinkedList<>();
        LinkedList<String> allNumbers = new LinkedList<>();
        String regex = "[0-9]*[02468]{3}[0-9]*";
        while (tenNumbers.size() < 10) {
            String currentNumber = String.valueOf(random.nextInt(Integer.MAX_VALUE));
            allNumbers.add(currentNumber);
            if (!Pattern.matches(regex, currentNumber)) {
                tenNumbers.add(currentNumber);
            }
        }
        System.out.println(tenNumbers);
        System.out.println(allNumbers);
    }
}
