package com.alex.homework.readingsourcefile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ArchivingThread extends Thread {
    private String text;

    public ArchivingThread(String text) {
        this.text = text;
    }

    @Override
    public void run() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("output.zip");
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
            ByteBuffer buffer = ByteBuffer.wrap(text.getBytes());
            buffer.flip();
            zipOutputStream.putNextEntry(new ZipEntry(text));
            zipOutputStream.write(buffer.array());
            zipOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
