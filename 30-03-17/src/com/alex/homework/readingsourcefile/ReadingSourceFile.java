package com.alex.homework.readingsourcefile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ReadingSourceFile {
    public static void main(String[] args) {
        String filename = "Test.java";
        File file = new File(filename);

        String text = "";

        try {
            FileChannel fileChannel = new FileInputStream(file).getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1024);

            while(fileChannel.read(buffer) != -1) {
                buffer.flip();
                byte[] bytes = buffer.array();

                for (byte aByte : bytes) {
                    text += (char) aByte;
                }
                buffer.clear();
            }

            ChangingSourceFileThread changingSourceFileThread = new ChangingSourceFileThread(text);
            changingSourceFileThread.start();

            try {
                changingSourceFileThread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

            System.out.println(changingSourceFileThread.getText());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        Thread archivingThread = new ArchivingThread(text);
        archivingThread.start();

        try {
            archivingThread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
