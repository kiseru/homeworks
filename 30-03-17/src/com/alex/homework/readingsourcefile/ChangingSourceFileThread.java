package com.alex.homework.readingsourcefile;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ChangingSourceFileThread extends Thread {
    private String text;

    public ChangingSourceFileThread(String text) {
        this.text = text;
    }

    @Override
    public void run() {
        List<String> lines = Arrays.stream(text.split("[\n\r]"))
                .filter(word -> !word.equals(""))
                .collect(Collectors.toList());

        Thread deletingAnnotationsThread = new DeletingAnnotationsThread(lines);
        deletingAnnotationsThread.start();

        Thread deletingSingleLineCommentsThread = new DeletingSingleLineCommentsThread(lines);
        deletingSingleLineCommentsThread.start();

        Thread deletingMultiLinesCommentsThread = new DeletingMultiLinesCommentsThread(lines);
        deletingMultiLinesCommentsThread.start();

        Thread toUpperCase = new ToUpperCaseThread(lines);
        toUpperCase.start();

        try {
            deletingAnnotationsThread.join();
            deletingSingleLineCommentsThread.join();
            deletingMultiLinesCommentsThread.join();
            toUpperCase.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        text = lines.stream()
                .filter(line -> !line.equals(""))
                .reduce("", (acc, line) -> acc + "\n\r" + line);
    }

    public String getText() {
        return text;
    }
}
