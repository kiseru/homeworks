package com.alex.homework.readingsourcefile;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class DeletingMultiLinesCommentsThread extends Thread {
    private List<String> lines;

    public DeletingMultiLinesCommentsThread(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public void run() {
        Lock lock = new ReentrantLock();

        boolean flag = false;

        for (int i = 0; i < lines.size(); i++) {
            lock.lock();

            String line = lines.get(i);

            flag = Pattern.matches("[ ]*/\\*[ ]*[\r\n]*", line) || flag;

            if (flag) {
                lines.set(i, "");
            }

            flag = !Pattern.matches("[ ]*\\*/[ ]*[\r\n]*", line) && flag;

            lock.unlock();
        }
    }
}
