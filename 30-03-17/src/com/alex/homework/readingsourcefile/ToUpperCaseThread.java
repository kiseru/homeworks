package com.alex.homework.readingsourcefile;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ToUpperCaseThread extends Thread {
    private List<String> lines;

    public ToUpperCaseThread(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public void run() {
        Lock lock = new ReentrantLock();
        for (int i = 0; i < lines.size(); i++) {
            lock.lock();
            lines.set(i, lines.get(i).toUpperCase());
            lock.unlock();
        }
    }
}
