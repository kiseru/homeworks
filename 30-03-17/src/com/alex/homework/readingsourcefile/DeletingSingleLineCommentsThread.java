package com.alex.homework.readingsourcefile;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class DeletingSingleLineCommentsThread extends Thread {
    private List<String> lines;

    public DeletingSingleLineCommentsThread(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public void run() {
        Lock lock = new ReentrantLock();

        for (int i = 0; i < lines.size(); i++) {
            lock.lock();
            if (Pattern.matches("[ ]*[\\/]{2}[ ]*[\\w ]+[\r\n]*", lines.get(i))) {
                lines.set(i, "");
            }
            lock.unlock();
        }
    }
}
