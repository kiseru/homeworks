package com.zarina.actions;

public class Multiplication implements IOperation {
    @Override
    public double calculate(double a, double b) {
        return a * b;
    }
}
