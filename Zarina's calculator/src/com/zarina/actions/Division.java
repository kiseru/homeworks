package com.zarina.actions;

public class Division implements IOperation {
    @Override
    public double calculate(double a, double b) {
        return a / b;
    }
}
