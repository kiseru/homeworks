package com.zarina.actions;

public class Degree implements IOperation {
    @Override
    public double calculate(double a, double b) {
        return Math.pow(a, b);
    }
}
