package com.zarina.actions;

public class Addition implements IOperation {
    @Override
    public double calculate(double a, double b) {
        return a + b;
    }
}
