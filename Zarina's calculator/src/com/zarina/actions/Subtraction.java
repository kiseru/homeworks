package com.zarina.actions;

public class Subtraction implements IOperation {
    @Override
    public double calculate(double a, double b) {
        return a - b;
    }
}
