package com.zarina.actions;

public interface IOperation {
    double calculate(double a, double b);
}
