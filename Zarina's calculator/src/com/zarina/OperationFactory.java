package com.zarina;

import com.zarina.actions.*;

public class OperationFactory {
    public static IOperation getOperation(String sign) throws Exception {
        IOperation operation;
        switch (sign) {
            case "+":
                operation = new Addition();
                break;
            case "-":
                operation = new Subtraction();
                break;
            case "*":
                operation = new Multiplication();
                break;
            case "/":
                operation = new Division();
                break;
            case "^":
                operation = new Degree();
                break;
            default:
                throw new Exception("This action is not found");
        }
        return operation;
    }
}
