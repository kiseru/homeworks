package com.zarina;

import com.zarina.actions.IOperation;

public class Calculator {
    public static double calculator(double a, String act, double b) throws Exception {
        IOperation operation = OperationFactory.getOperation(act);
        return operation.calculate(a, b);
    }
}
