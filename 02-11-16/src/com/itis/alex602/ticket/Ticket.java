package com.itis.alex602.ticket;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Ticket {
    private final byte n = 6;
    private byte[] ticket = new byte[n];

    public Ticket(int _number) throws Exception {
        if (_number >= 0 && _number <= 999999) {
            for (int i = n - 1; i >= 0; i++) {
                ticket[i] = (byte)(_number % 10);
                _number /= 10;
            }
        } else throw new Exception("Ticket must have 6 numbers and be natural");
    }

    public boolean isPalindrome() {
        boolean _isPalindrome = true;
        for (int i = 0; i < n / 2 && _isPalindrome; i++) {
            if (ticket[i] != ticket[5 - i]) _isPalindrome = false;
        }
        return _isPalindrome;
    }

    public boolean isLuckyByMoscow() {
        return ticket[0] + ticket[1] + ticket[2] == ticket[3] + ticket[4] + ticket[5];
    }

    public boolean isLuckyBySaintPetersburg() {
        return ticket[0] + ticket[2] + ticket[4] == ticket[1] + ticket[3] + ticket[5];
    }

    public boolean isNearWithLuckyByMoscow() {
        return Math.abs(ticket[0] + ticket[1] + ticket[2] - ticket[3] + ticket[4] + ticket[5]) == 1;
    }

    public boolean isNearWithLuckyBySaintPetersburg() {
        return Math.abs(ticket[0] + ticket[2] + ticket[4] - ticket[1] + ticket[3] + ticket[5]) == 1;
    }
}
