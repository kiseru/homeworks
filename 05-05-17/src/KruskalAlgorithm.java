import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class KruskalAlgorithm {

    private static int cost;
    private static List<Pair<Integer, Integer>> result;

    public static void run(int[][] matrix) {

        // вес - первая вершина - вторая вершина
        LinkedList<Pair<Integer, Pair<Integer, Integer>>> g = new LinkedList<>();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = i + 1; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) continue;

                Pair<Integer, Integer> vertexesPair = new Pair<>(i, j);

                g.add(new Pair<>(matrix[i][j], vertexesPair));
            }
        }

        g.sort(Comparator.comparingInt(Pair::getKey));

        result = new LinkedList<>();

        int[] treeId = IntStream.iterate(0, i -> i + 1).limit(matrix.length).toArray();

        for (Pair<Integer, Pair<Integer, Integer>> pair : g) {
            int first = pair.getValue().getKey();
            int second = pair.getValue().getValue();
            int weight = pair.getKey();

            if (treeId[first] != treeId[second]) {
                cost += weight;
                result.add(new Pair<>(first, second));
                int oldId = treeId[second];
                int newId = treeId[first];
                treeId = Arrays.stream(treeId)
                        .map(id -> id = id == oldId ? newId : id)
                        .toArray();
            }
        }

        result.forEach(System.out::println);
        System.out.println(cost);
    }

    public static int getCost() {
        return cost;
    }

    public static List<Pair<Integer, Integer>> getResult() {
        return result;
    }
}

class Pair<K, V> {

    private K key;
    private V value;

    Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    K getKey() {
        return key;
    }

    V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%s - %s", key, value);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Pair && ((Pair) obj).getKey().equals(this.key) && ((Pair) obj).getValue().equals(this.value);
    }
}