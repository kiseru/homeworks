import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class KruskalAlgorithmTest {

    private static Class kruskalAlgorithmClass;
    private static Method runMethod;

    @BeforeClass
    public static void initialize() throws NoSuchMethodException {
        kruskalAlgorithmClass = KruskalAlgorithm.class;
        runMethod = kruskalAlgorithmClass.getMethod("run", int[][].class);
    }

    @Test
    public void test() {
        int[][] matrix = new int[][] {
                { 0, 3, 0, 0, 6, 5 },
                { 3, 0, 1, 0, 0, 4 },
                { 0, 1, 0, 6, 0, 4 },
                { 0, 0, 6, 0, 8, 5 },
                { 6, 0, 0, 8, 0, 2 },
                { 5, 4, 4, 5, 2, 0 }
        };

        KruskalAlgorithm.run(matrix);

        LinkedList<Pair<Integer, Integer>> expected = new LinkedList<>();
        expected.add(new Pair<>(0, 1));
        expected.add(new Pair<>(1, 2));
        expected.add(new Pair<>(1, 5));
        expected.add(new Pair<>(4, 5));
        expected.add(new Pair<>(3, 5));

        List<Pair<Integer, Integer>> result = KruskalAlgorithm.getResult();

        boolean firstTest = true;
        for (int i = 0; i < expected.size() && firstTest; i++) {
            boolean tempTest = false;
            for (int j = 0; j < result.size() && !tempTest; j++) {
                tempTest = expected.get(i).equals(result.get(j));
            }
            firstTest = tempTest;
        }

        boolean secondTest = KruskalAlgorithm.getCost() == 15;

        assertTrue(firstTest && secondTest);
    }
}