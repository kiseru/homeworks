package com.alex;

import com.alex.sorts.BinTreeSort;
import com.alex.sorts.MergeSort;
import com.alex.sorts.QuickSort;
import com.alex.sorts.Sort;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int[] array = {1, 3, 4, 5, 7, 2, 6};
        System.out.println(Arrays.toString(array));
        Sort sort = BinTreeSort.getSort();
        array = sort.sort(array);
        System.out.println(Arrays.toString(array));
    }
}