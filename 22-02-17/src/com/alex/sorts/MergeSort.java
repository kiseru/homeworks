package com.alex.sorts;

import java.util.Arrays;

public class MergeSort extends Sort {
    private static MergeSort sort;

    private int[] array;

    private MergeSort() {}

    public static MergeSort getSort() {
        if (sort == null) {
            synchronized (MergeSort.class) {
                if (sort == null) {
                    sort = new MergeSort();
                }
            }
        }

        return sort;
    }

    @Override
    public int[] sort(int[] array) {
        doSort(array);
        return array;
    }

    private void doSort(int[] array) {
        if (array.length < 2) return;

        int medium = array.length / 2;

        int[] firstArray;
        int[] secondArray;

        firstArray = Arrays.copyOfRange(array, 0, medium);
        secondArray = Arrays.copyOfRange(array, medium, array.length);

        doSort(firstArray);
        doSort(secondArray);

        int firstPoint = 0;
        int secondPoint = 0;

        for (int i = 0; i < array.length; i++) {
            if (firstPoint < firstArray.length && secondPoint < secondArray.length) {
                if (firstArray[firstPoint] < secondArray[secondPoint]) {
                    array[i] = firstArray[firstPoint];
                    firstPoint++;
                } else {
                    array[i] = secondArray[secondPoint];
                    secondPoint++;
                }
            } else if (firstPoint < firstArray.length) {
                array[i] = firstArray[firstPoint];
                firstPoint++;
            } else if (secondPoint < secondArray.length) {
                array[i] = secondArray[secondPoint];
                secondPoint++;
            }
        }
    }
}