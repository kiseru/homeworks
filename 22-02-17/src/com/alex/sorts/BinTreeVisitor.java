package com.alex.sorts;

import java.util.LinkedList;

public class BinTreeVisitor implements IBinTreeVisitor {
    private LinkedList<Integer> array;

    public BinTreeVisitor() {
        array = new LinkedList<>();
    }

    public int[] getArray() {
        int[] result = new int[array.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = array.get(i);
        }
        return result;
    }

    @Override
    public void visit(BinTree binTree) {
        array.add(binTree.getRoot());
    }
}