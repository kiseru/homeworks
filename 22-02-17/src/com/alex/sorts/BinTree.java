package com.alex.sorts;

public class BinTree {
    int root;
    BinTree left;
    BinTree right;

    public BinTree(int root) {
        this.root = root;
    }

    public int getRoot() {
        return root;
    }

    public BinTree add(int value) {
        if (value < root) {
            if (left == null) {
                left = new BinTree(value);
            } else {
                left.add(value);
            }
        } else {
            if (right == null) {
                right = new BinTree(value);
            } else {
                right.add(value);
            }
        }
        return this;
    }

    public void accept(IBinTreeVisitor visitor) {
        if (left != null) {
            left.accept(visitor);
        }

        visitor.visit(this);

        if (right != null) {
            right.accept(visitor);
        }
    }
}