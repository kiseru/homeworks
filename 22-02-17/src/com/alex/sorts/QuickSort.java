package com.alex.sorts;

public class QuickSort extends Sort {
    private static QuickSort sort;

    private QuickSort() {}

    public static QuickSort getSort() {
        if (sort == null) {
            synchronized (QuickSort.class) {
                if (sort == null) {
                    sort = new QuickSort();
                }
            }
        }

        return sort;
    }

    @Override
    public int[] sort(int[] array) {
        int left = 0;
        int right = array.length - 1;

        doSort(array, left, right);

        return array;
    }

    private void doSort(int[] array, int left, int right) {
        if (left >= right) return;

        int i = left;
        int j = right;
        int medium = (left + right) / 2;

        while (i < j) {
            while (array[i] <= array[medium] && i + 1 <= medium) i++;
            while (array[j] > array[medium] && j - 1 >= medium) j--;

            if (i < j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        doSort(array, left, medium);
        doSort(array, medium + 1, right);
    }
}