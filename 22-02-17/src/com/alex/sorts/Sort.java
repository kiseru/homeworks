package com.alex.sorts;

public abstract class Sort {
    public abstract int[] sort(int[] array);
}
