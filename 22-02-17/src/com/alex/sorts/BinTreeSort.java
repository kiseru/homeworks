package com.alex.sorts;

public class BinTreeSort extends Sort {
    private static BinTreeSort sort;

    private BinTreeSort() {}

    public static BinTreeSort getSort() {
        if (sort == null) {
            synchronized (BinTreeSort.class) {
                if (sort == null) {
                    sort = new BinTreeSort();
                }
            }
        }

        return sort;
    }

    @Override
    public int[] sort(int[] array) {
        BinTree binTree = new BinTree(array[0]);

        for (int i = 1; i < array.length; i++) {
            binTree.add(array[i]);
        }

        BinTreeVisitor visitor = new BinTreeVisitor();

        binTree.accept(visitor);
        return visitor.getArray();
    }
}