package com.alex.homework;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String text = "Hi! My name is Alexander! And I glad to see you!";
        System.out.println(getWords(text));
        System.out.println(getWordWithPopularLetters(getWords(text)));
    }

    private static List<String> getWords(String text) {
        String[] wordsArray = text.split("\\s");
        return Arrays.stream(wordsArray)
                .map(word -> {
                    if (Pattern.matches("[A-za-z]+\\W$", word)) {
                        return word.substring(0, word.length() - 1);
                    }
                    return word;
                })
                .filter(word -> Pattern.matches("[A-Za-z]+", word))
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }

    private static String getWordWithPopularLetters(List<String> words) {
        Map<Integer, HashMap<Character, Integer>> counter = new HashMap<>();
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                if (counter.get(i) == null) {
                    Character letter = word.charAt(i);
                    counter.put(i, new HashMap<>());
                    counter.get(i).put(letter, 1);
                } else {
                    if (counter.get(i).get(word.charAt(i)) == null) {
                        counter.get(i).put(word.charAt(i), 1);
                    } else {
                        counter.get(i).put(word.charAt(i), counter.get(i).get(word.charAt(i)) + 1);
                    }
                }
            }
        }

        String result = "";
        for (int i = 0; i < counter.size(); i++) {
            Character popularLetter = ' ';
            int letterCount = 0;
            Set<Map.Entry<Character, Integer>> entrySet = counter.get(i).entrySet();
            for (Map.Entry<Character, Integer> entry : entrySet) {
                if (entry.getValue() > letterCount) {
                    popularLetter = entry.getKey();
                    letterCount = entry.getKey();
                }
            }
            result += popularLetter.toString();
        }
        return result;
    }
}
