public class Test {
    public static void main(String[] args) {
        IAVLBinTree binTree = new AVLBinTree(5);
        binTree = binTree.add(1);
        binTree = binTree.add(3);
        binTree = binTree.add(6);
        binTree = binTree.add(7);
        binTree = binTree.add(2);
        binTree = binTree.add(9);
        binTree = binTree.add(8);
        binTree = binTree.add(4);

        binTree.show();
    }
}