public class EmptyAVLBinTree implements IAVLBinTree {
    private static EmptyAVLBinTree binTree;

    private int height;

    private EmptyAVLBinTree() {
        height = 0;
    }

    public static EmptyAVLBinTree getBinTree() {
        if (binTree == null) {
            synchronized (EmptyAVLBinTree.class) {
                if (binTree == null) {
                    binTree = new EmptyAVLBinTree();
                }
            }
        }

        return binTree;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public AVLBinTree add(int value) {
        return new AVLBinTree(value);
    }

    @Override
    public void show() {}
}