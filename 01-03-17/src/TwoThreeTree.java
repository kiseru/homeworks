public class TwoThreeTree implements ITwoThreeTree {
    private Integer firstKey;
    private Integer secondKey;

    private boolean isFull;

    private ITwoThreeTree left;
    private ITwoThreeTree right;

    public TwoThreeTree(int key) {
        firstKey = key;
        isFull = false;
        left = EmptyTwoThreeTree.getTwoThreeTree();
        right = EmptyTwoThreeTree.getTwoThreeTree();
    }

    @Override
    public ITwoThreeTree add(int value) {
        if (secondKey == null) {
            if (value < firstKey) {
                secondKey = firstKey;
                firstKey = value;
            } else {
                secondKey = value;
            }
        } else if (isFull) {
            if (value < firstKey) {
                left = left.add(value);
            } else {
                right = right.add(value);
            }
        } else {
            if (value < firstKey) {
                left = new TwoThreeTree(value);
                right = new TwoThreeTree(secondKey);
                isFull = true;
            } else if (value > secondKey) {
                left = new TwoThreeTree(firstKey);
                right = new TwoThreeTree(value);
                firstKey = secondKey;
            } else {
                left = new TwoThreeTree(firstKey);
                right = new TwoThreeTree(secondKey);
                firstKey = value;
            }
        }

        return this;
    }
}