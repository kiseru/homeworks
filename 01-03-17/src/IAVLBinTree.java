public interface IAVLBinTree {
    int getHeight();
    IAVLBinTree add(int value);
    void show();
}
