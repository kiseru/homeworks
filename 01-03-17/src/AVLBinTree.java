public class AVLBinTree implements IAVLBinTree {
    private int root;
    private int height;

    private IAVLBinTree left;
    private IAVLBinTree right;

    public AVLBinTree(int root) {
        this.root = root;
        height = 1;
        left = EmptyAVLBinTree.getBinTree();
        right = EmptyAVLBinTree.getBinTree();
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public IAVLBinTree add(int value) {
        if (value < root) {
            left = left.add(value);
        } else {
            right = right.add(value);
        }

        return balance();
    }

    private AVLBinTree balance() {
        fixHeight();

        if (balanceFactor() == 2) {
            AVLBinTree left = (AVLBinTree) this.left;
            if (left.balanceFactor() < 0) {
                this.left = left.rotateLeft();
            }

            return rotateRight();
        }

        if (balanceFactor() == -2) {
            AVLBinTree right = (AVLBinTree) this.right;
            if (right.balanceFactor() > 0) {
                this.right = right.rotateRight();
            }

            return rotateLeft();
        }

        return this;
    }

    private AVLBinTree rotateRight() {
        AVLBinTree temp;

        try {
            temp = (AVLBinTree) left;
        } catch (ClassCastException e) {
            return this;
        }

        left = temp.right;
        temp.right = this;

        fixHeight();
        temp.fixHeight();

        return temp;
    }

    private AVLBinTree rotateLeft() {
        AVLBinTree temp;

        try {
            temp = (AVLBinTree) right;
        } catch (ClassCastException e) {
            return this;
        }

        right = temp.left;
        temp.left = this;

        fixHeight();
        temp.fixHeight();

        return temp;
    }

    private void fixHeight() {
        int leftHeight = left.getHeight();
        int rightHeight = right.getHeight();

        height = leftHeight > rightHeight ? leftHeight : rightHeight;
        height++;
    }

    private int balanceFactor() {
        return left.getHeight() - right.getHeight();
    }

    @Override
    public void show() {
        System.out.println(root);
        left.show();
        right.show();
    }
}