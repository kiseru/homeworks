import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class BinWarshallTest {

    @Test
    void testBinWarshallAlg() {
        int[] matrix = new int[] { 0b0100, 0b0001, 0b0000, 0b1010 };

        BinWarshall.run(matrix);

        boolean test = matrix[0] == 0b1111;
        test = test && matrix[1] == 0b1111;
        test = test && matrix[2] == 0b0000;
        test = test && matrix[3] == 0b1111;

        assertTrue(test);
    }

}