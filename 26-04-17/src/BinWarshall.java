public class BinWarshall {
    public static void run(int[] binArray) {
        for (int i = 0; i < binArray.length; i++) {
            for (int j = 0; j < binArray.length; j++) {
                int temp = pow(2,binArray.length - j - 1);
                int newNumber = 0;
                for (int k = 0; k < binArray.length; k++) {
                    newNumber += binArray[j] & temp;
                }

                if ((newNumber & binArray[i]) > 0) {
                    binArray[i] += temp;
                }
            }
        }
    }

    private static int pow(int a, int b) {
        if (b == 0) {
            return 1;
        } else if (b == 1) {
            return a;
        } else if (b % 2 == 0) {
            int temp = pow(a, b / 2);
            return temp * temp;
        } else {
            return pow(a, b - 1) * a;
        }
    }
}
