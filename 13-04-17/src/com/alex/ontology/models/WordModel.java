package com.alex.ontology.models;

import com.alex.ontology.database.PostgreSQLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class WordModel {
    private int id;
    private String word;
    private int partOfSpeechID;

    private static int size;
    private static Connection connection;

    static {
        connection = PostgreSQLConnection.getPostgreSQLConnection().getConnection();
        try {
            Statement statement = connection.createStatement();
            String sqlQuery = "select count(*) from words;";
            ResultSet sqlResult = statement.executeQuery(sqlQuery);
            sqlResult.next();
            size = sqlResult.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public WordModel(String word, String partOfSpeech) throws SQLException {
        this(0, word, PartOfSpeechModel.getIDByName(partOfSpeech));
    }

    private WordModel(int id, String word, int partOfSpeechID) throws SQLException {
        this.id = id;
        this.word = word;
        this.partOfSpeechID = partOfSpeechID;
    }

    public WordModel(ResultSet result) throws SQLException {
        id = result.getInt("id");
        word = result.getString("word");
        partOfSpeechID = result.getInt("part_of_speech_id");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getPartOfSpeechID() {
        return partOfSpeechID;
    }

    public void setPartOfSpeechID(String partOfSpeech) throws SQLException {
        this.partOfSpeechID = PartOfSpeechModel.getIDByName(partOfSpeech);
    }

    public void save() throws SQLException {
        Statement statement = connection.createStatement();
        size++;
        id = size;
        String sqlQuery = String.format("insert into words (id, word, part_of_speech) values (%d, '%s', '%s');", id, word, partOfSpeechID);
        statement.executeUpdate(sqlQuery);
    }

    public void update() throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = String.format("update words set word = '%s', part_of_speech = '%s' where id = %d;", word, partOfSpeechID, id);
        statement.executeUpdate(sqlQuery);
    }

    public void remove() throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = String.format("delete from words where id = %d", id);
        statement.executeUpdate(sqlQuery);
    }

    public static List<WordModel> getAll() throws SQLException {
        String sqlQuery = "select * from words";

        Statement statement = connection.createStatement();
        ResultSet queryResult = statement.executeQuery(sqlQuery);

        List<WordModel> words = new LinkedList<>();
        while (queryResult.next()) {
            WordModel word = new WordModel(queryResult);
            words.add(word);
        }

        return words;
    }

    public static List<WordModel> getAdjectives() throws SQLException {
        int adjectivesID = PartOfSpeechModel.getIDByName("adjective");

        String sqlQuery = String.format("select * from words where part_of_speech_id = %d;", adjectivesID);
        Statement statement = connection.createStatement();
        ResultSet queryResult = statement.executeQuery(sqlQuery);

        List<WordModel> adjectives = new LinkedList<>();
        while (queryResult.next()) {
            WordModel adjective = new WordModel(queryResult);
            adjectives.add(adjective);
        }

        return adjectives;
    }

    public static List<WordModel> getNouns() throws SQLException {
        int nounsID = PartOfSpeechModel.getIDByName("noun");

        String sqlQuery = String.format("select * from words where part_of_speech_id = %d;", nounsID);
        Statement statement = connection.createStatement();
        ResultSet queryResult = statement.executeQuery(sqlQuery);

        List<WordModel> nouns = new LinkedList<>();
        while (queryResult.next()) {
            WordModel noun = new WordModel(queryResult);
            nouns.add(noun);
        }

        return nouns;
    }

    public static List<WordModel> getAdverbs() throws SQLException {
        int adverbsID = PartOfSpeechModel.getIDByName("adverb");

        String sqlQuery = String.format("select * from words where part_of_speech_id = %d;", adverbsID);
        Statement statement = connection.createStatement();
        ResultSet queryResult = statement.executeQuery(sqlQuery);

        List<WordModel> adverbs = new LinkedList<>();
        while (queryResult.next()) {
            WordModel adverb = new WordModel(queryResult);
            adverbs.add(adverb);
        }

        return adverbs;
    }

    public static List<WordModel> getVerbs() throws SQLException {
        int verbsID = PartOfSpeechModel.getIDByName("verb");

        String sqlQuery = String.format("select * from words where part_of_speech_id = %d;", verbsID);
        Statement statement = connection.createStatement();
        ResultSet queryResult = statement.executeQuery(sqlQuery);

        List<WordModel> verbs = new LinkedList<>();
        while (queryResult.next()) {
            WordModel verb = new WordModel(queryResult);
            verbs.add(verb);
        }

        return verbs;
    }
}
