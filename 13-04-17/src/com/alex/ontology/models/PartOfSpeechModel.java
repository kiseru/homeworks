package com.alex.ontology.models;

import com.alex.ontology.database.PostgreSQLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class PartOfSpeechModel {
    private int id;
    private String partOfSpeech;

    private static int size;
    private static Connection connection;

    static {
        connection = PostgreSQLConnection.getPostgreSQLConnection().getConnection();

        try {
            Statement statement = connection.createStatement();
            String sqlQuery = "select count(*) from parts_of_speech;";
            ResultSet result = statement.executeQuery(sqlQuery);
            if (result.next()) {
                size = result.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PartOfSpeechModel(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    private PartOfSpeechModel(int id, String partOfSpeech) {
        this.id = id;
        this.partOfSpeech = partOfSpeech;
    }

    public void setID() {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void save() throws SQLException {
        Statement statement = connection.createStatement();
        size++;
        this.id = size;
        String sqlQuery = "insert into parts_of_speech (id, part_of_speech) values (" + size + ", '" + partOfSpeech + "');";
        statement.executeUpdate(sqlQuery);
    }

    public void update() throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = String.format("update parts_of_speech set part_of_speech = '%s' where id = %d;", partOfSpeech, id);
        statement.executeUpdate(sqlQuery);
    }

    public static PartOfSpeechModel find(int id) throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = String.format("select * from parts_of_speech where id = %d;", id);
        ResultSet result = statement.executeQuery(sqlQuery);
        result.next();
        int newID = result.getInt("id");
        String partOfSpeech = result.getString("part_of_speech");
        return new PartOfSpeechModel(newID, partOfSpeech);
    }

    public static int getIDByName(String partOfSpeech) throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = String.format("select * from parts_of_speech where part_of_speech = '%s';", partOfSpeech);
        ResultSet result = statement.executeQuery(sqlQuery);
        result.next();
        return result.getInt("id");
    }

    public void remove() throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = String.format("delete from parts_of_speech where id = %d;", id);
        statement.executeUpdate(sqlQuery);
        id = 0;
    }

    public static List<PartOfSpeechModel> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        String sqlQuery = "select * from parts_of_speech";
        ResultSet sqlResult = statement.executeQuery(sqlQuery);

        List<PartOfSpeechModel> list = new LinkedList<>();
        while (sqlResult.next()) {
            int id = sqlResult.getInt("id");
            String partOfSpeech = sqlResult.getString("part_of_speech");
            list.add(new PartOfSpeechModel(id, partOfSpeech));
        }

        return list;
    }
}
