package com.alex.ontology.models;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PartOfSpeechModelTest {
    private PartOfSpeechModel partOfSpeech;

    @BeforeEach
    void initialize() throws SQLException {
        partOfSpeech = new PartOfSpeechModel("test");
        partOfSpeech.save();
    }

    @AfterEach
    void destroying() throws SQLException {
        partOfSpeech.remove();
    }

    @Test
    void findingTest() throws SQLException {
        int id = partOfSpeech.getID();
        PartOfSpeechModel partOfSpeech = PartOfSpeechModel.find(id);
        assertEquals(partOfSpeech.getPartOfSpeech(), this.partOfSpeech.getPartOfSpeech());
    }

    @Test
    void savingTest() throws SQLException {
        assertNotEquals(partOfSpeech.getID(), 0);
    }

    @Test
    void removingTest() throws SQLException {
        partOfSpeech.remove();
        assertEquals(partOfSpeech.getID(), 0);
        partOfSpeech.save();
    }

    @Test
    void updatingTest() throws SQLException {
        partOfSpeech.setPartOfSpeech("updateTest");
        partOfSpeech.update();
        partOfSpeech = PartOfSpeechModel.find(partOfSpeech.getID());
        assertEquals(partOfSpeech.getPartOfSpeech(), "updateTest");
    }
}