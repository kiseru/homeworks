package com.alex.ontology.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreSQLConnection {
    private static PostgreSQLConnection postgreSQLConnection;

    private Connection connection;

    private PostgreSQLConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/alex",
                    "alex", "big_secret");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static PostgreSQLConnection getPostgreSQLConnection() {
        if (postgreSQLConnection == null) {
            synchronized (PostgreSQLConnection.class) {
                if (postgreSQLConnection == null) {
                    postgreSQLConnection = new PostgreSQLConnection();
                }
            }
        }

        return postgreSQLConnection;
    }

    public Connection getConnection() {
        return connection;
    }
}
