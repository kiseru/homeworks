package com.alex.ontology.generators;

import com.alex.ontology.models.WordModel;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class AdjectivesGenerator implements WordsGenerator {
    private static List<WordModel> adjectives;
    private static final Random random;

    static {
        random = new Random();

        try {
            adjectives = WordModel.getAdjectives();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getWord() {
        int indexOfWord = random.nextInt(adjectives.size());
        return adjectives.get(indexOfWord).getWord();
    }
}