package com.alex.ontology.generators;

import com.alex.ontology.models.WordModel;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class AdverbsGenerator implements WordsGenerator {
    private static List<WordModel> adverbs;
    private static final Random random;

    static {
        random = new Random();

        try {
            adverbs = WordModel.getAdverbs();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getWord() {
        int adverbIndex = random.nextInt(adverbs.size());
        return adverbs.get(adverbIndex).getWord();
    }
}
