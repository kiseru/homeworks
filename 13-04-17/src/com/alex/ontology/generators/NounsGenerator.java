package com.alex.ontology.generators;

import com.alex.ontology.models.WordModel;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class NounsGenerator implements WordsGenerator {
    private static List<WordModel> nouns;
    private static final Random random;

    static {
        random = new Random();

        try {
            nouns = WordModel.getNouns();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getWord() {
        int nounIndex = random.nextInt(nouns.size());
        return nouns.get(nounIndex).getWord();
    }
}
