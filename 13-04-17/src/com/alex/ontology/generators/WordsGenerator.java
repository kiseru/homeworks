package com.alex.ontology.generators;

public interface WordsGenerator {
    String getWord();
}
