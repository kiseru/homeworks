package com.alex.ontology.generators;

import com.alex.ontology.models.WordModel;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class VerbsGenerator implements WordsGenerator {
    private static List<WordModel> verbs;
    private static final Random random;

    static {
        random = new Random();

        try {
            verbs = WordModel.getVerbs();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getWord() {
        int verbIndex = random.nextInt(verbs.size());
        return verbs.get(verbIndex).getWord();
    }
}
