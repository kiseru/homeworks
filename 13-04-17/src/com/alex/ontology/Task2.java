package com.alex.ontology;

import com.alex.ontology.generators.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) throws IOException {
        FileChannel fileWriterChannel = new FileOutputStream("task2.txt").getChannel();

        List<WordsGenerator> wordsGeneratorList = new ArrayList<>();
        wordsGeneratorList.add(new AdjectivesGenerator());
        wordsGeneratorList.add(new NounsGenerator());
        wordsGeneratorList.add(new VerbsGenerator());
        wordsGeneratorList.add(new AdverbsGenerator());

        for (int i = 0; i < 1000; i++) {
            String word = wordsGeneratorList.get(i % wordsGeneratorList.size()).getWord() + " ";
            fileWriterChannel.write(ByteBuffer.wrap(word.getBytes()));

            if ((i + 1) % 4 == 0) {
                fileWriterChannel.write(ByteBuffer.wrap("\n".getBytes()));
            }
        }

        fileWriterChannel.close();
    }
}
