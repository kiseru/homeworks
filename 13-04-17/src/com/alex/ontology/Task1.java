package com.alex.ontology;

import com.alex.ontology.generators.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task1 {

    public static void main(String[] args) throws IOException, SQLException {
        File file = new File("task1.txt");
        FileChannel fileWriterChannel = new FileOutputStream(file).getChannel();

        List<WordsGenerator> wordsGeneratorList = new ArrayList<>();
        wordsGeneratorList.add(new AdjectivesGenerator());
        wordsGeneratorList.add(new NounsGenerator());
        wordsGeneratorList.add(new AdverbsGenerator());
        wordsGeneratorList.add(new VerbsGenerator());

        Random random = new Random();

        ByteBuffer buffer;
        for (int i = 0; i < 1000; i++) {
            int generatorIndex = random.nextInt(wordsGeneratorList.size());
            String word = wordsGeneratorList.get(generatorIndex).getWord() + " ";
            fileWriterChannel.write(ByteBuffer.wrap(word.getBytes()));

            if (i % 4 == 0 && i != 0) {
                fileWriterChannel.write(ByteBuffer.wrap("\n".getBytes()));
            }
        }

        fileWriterChannel.close();
    }
}
