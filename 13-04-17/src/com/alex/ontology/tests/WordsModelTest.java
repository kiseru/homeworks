package com.alex.ontology.tests;

import com.alex.ontology.models.WordModel;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class WordsModelTest {

    @Test
    void selectAdjectives() throws SQLException {
        List<WordModel> adjectives = WordModel.getAdjectives();

        boolean everyWordIsAdjective = true;
        for (WordModel adjective : adjectives) {
            everyWordIsAdjective = everyWordIsAdjective && adjective.getPartOfSpeechID() == 1;
        }

        assertTrue(everyWordIsAdjective);
    }

    @Test
    void selectNouns() throws SQLException {
        List<WordModel> nouns = WordModel.getNouns();

        boolean everyWordIsNoun = true;
        for (WordModel noun : nouns) {
            everyWordIsNoun = everyWordIsNoun && noun.getPartOfSpeechID() == 2;
        }

        assertTrue(everyWordIsNoun);
    }

    @Test
    void selectAdverbs() throws SQLException {
        List<WordModel> adverbs = WordModel.getAdverbs();

        boolean everyWordIsAdverb = true;
        for (WordModel adverb : adverbs) {
            everyWordIsAdverb = everyWordIsAdverb && adverb.getPartOfSpeechID() == 3;
        }

        assertTrue(everyWordIsAdverb);
    }

    @Test
    void selectVerbs() throws SQLException {
        List<WordModel> verbs = WordModel.getVerbs();

        boolean everyWordIsVerb = true;
        for (WordModel verb : verbs) {
            everyWordIsVerb = everyWordIsVerb && verb.getPartOfSpeechID() == 4;
        }

        assertTrue(everyWordIsVerb);
    }
}
