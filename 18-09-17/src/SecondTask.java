import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SecondTask {

    public static void main(String[] args) throws IOException {

        final String regex = "\\w=\\w(&\\w=\\w)*";
        Pattern pattern = Pattern.compile(regex);

        List<String> files =
                Files.lines(Paths.get("urls.txt"), StandardCharsets.UTF_8)
                        .map(line -> pattern.matcher(line).replaceAll(""))
                        .map(SecondTask::getParamsLineInfo)
                        .collect(Collectors.toList());
        files.forEach(System.out::println);
    }

    private static String getParamsLineInfo(String str) {
        String[] params = str.split("&");
        StringBuilder stringBuilder =
                new StringBuilder(String.format("PARAM STRING: %s; ", str));
        Arrays.stream(params)
                .forEach(param ->
                        stringBuilder.append(getParamInfo(param)));
        return stringBuilder.toString();
    }

    private static String getParamInfo(String str) {
        String[] map = str.split("=");
        return String.format("NAME: %s; VALUE: %s; ", map[0], map[1]);
    }
}
