import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FirstTask {

    public static void main(String[] args) throws IOException {
        final String regex = "\\w*/";
        Pattern pattern = Pattern.compile(regex);
        List<String> files =
                Files.lines(Paths.get("urls.txt"), StandardCharsets.UTF_8)
                .map(line -> pattern.matcher(line).replaceAll(""))
                .map(FirstTask::getFileInfo)
                .collect(Collectors.toList());
        files.forEach(System.out::println);
    }

    private static String getFileInfo(String filename) {
        String[] stringArray = filename.split("\\.");
        return String.format("FILENAME: %s; EXTENSION: %s;", stringArray[0], stringArray[1]);
    }
}
