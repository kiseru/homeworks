import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class ChangeOfCoinsTest {

    private int[] coins;
    private int amount;
    private List<Coins> expexted;

    @Before
    public void initialize() {}

    public ChangeOfCoinsTest(Helper helper) {
        this.coins = helper.coins;
        this.amount = helper.amount;
        this.expexted = helper.expected;
    }

    @Parameterized.Parameters
    public static Collection getTestsValues() {
        Collection<Helper> tests = new LinkedList<>();
        tests.add(new Helper(
                new int[] { 1, 2, 5, 10 },
                78,
                Arrays.asList(
                        new Coins(10, 7),
                        new Coins(5, 1),
                        new Coins(2, 1),
                        new Coins(1,1)
                )
        ));

        tests.add(new Helper(
                new int[] { 1, 4, 6 },
                8,
                Arrays.asList(
                        new Coins(4, 2)
                )
        ));

        return tests;
    }

    @Test
    public void test() {
        List<Coins> output = ChangeOfCoins.getChanging(coins, amount);
        assertTrue(
                output.size() == expexted.size()
                && output.containsAll(expexted)
                && expexted.containsAll(output)
        );
    }

    private static class Helper {
        int[] coins;
        int amount;
        List<Coins> expected;

        public Helper(int[] coins, int amount, List<Coins> expected) {
            this.coins = coins;
            this.amount = amount;
            this.expected = expected;
        }
    }
}