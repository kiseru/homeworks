import java.util.LinkedList;
import java.util.List;

public class ChangeOfCoins {

    public static List<Coins> getChanging(int[] coins, int amount) {

        if (amount < 1) return new LinkedList<>();

        List<Coins> result = new LinkedList<>();
        result.add(new Coins(1, amount));

        int count = amount;

        for (int nominal : coins) {
            if (nominal == 1) continue;

            if (amount < nominal) return result;

            List<Coins> newResult = getChanging(coins, amount % nominal);
            int newCount = (amount / nominal) + newResult.stream()
                    .mapToInt(c -> c.count)
                    .reduce(0, (acc, coinsCount) -> acc += coinsCount);

            if (newCount < count) {
                result = newResult;
                count = newCount;
                result.add(new Coins(nominal, amount / nominal));
            }
        }

        return result;
    }
}

class Coins {
    int nominal;
    int count;

    Coins(int nominal, int count) {
        this.nominal = nominal;
        this.count = count;
    }

    private boolean isEqual(Coins coins) {
        return nominal == coins.nominal && count == coins.count;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Coins
                && ((Coins) obj).isEqual(this);
    }

    @Override
    public String toString() {
        return String.format("[%d: %d]", nominal, count);
    }
}
