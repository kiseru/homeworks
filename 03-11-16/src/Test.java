import com.itis.alex602.LongNumber.UnsignedLongInt;

import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Test {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        UnsignedLongInt first = new UnsignedLongInt(scanner.next());
        System.out.println(first.shiftLeft(scanner.nextInt()));
    }
}
