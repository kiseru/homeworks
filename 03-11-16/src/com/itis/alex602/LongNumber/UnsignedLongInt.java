package com.itis.alex602.LongNumber;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class UnsignedLongInt extends LongInt {
    public UnsignedLongInt() {
        this(0);
    }

    public UnsignedLongInt(int value) {
        if (value == 0) {
            number = new Array();
            number.add();
        } else {
            number = new Array();
            for (int i = 0; value > 0; i++) {
                int temp = value % n;
                number.add(temp);
                value /= n;
            }
        }
    }

    public UnsignedLongInt(String value) {
        number = new Array();
        for (int i = value.length() - 1; i >= 0; i -= 4) {
            String element = "";
            for (int j = 0; j < 4 && i - j >= 0; j++) {
                element = value.charAt(i - j) + element;
            }
            number.add(Integer.parseInt(element));
        }
    }

    @Override
    public UnsignedLongInt add(ILongNumber _number) {
        try {
            if (!(_number instanceof UnsignedLongInt)) throw new Exception("The Object must be of UnsignedLongInt");
            UnsignedLongInt temp = (UnsignedLongInt) _number;
            for (int i = 0; i < number.length() && i < temp.number.length(); i++) {
                while (true) {
                    try {
                        number.get(i);
                        break;
                    } catch (ArrayIndexOutOfBoundsException e) {
                        number.add(0);
                    }
                }
                number.set(number.get(i) + temp.number.get(i), i);
            }
            return reducePos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public ILongNumber sub(ILongNumber _number) {
        try {
            if (!(_number instanceof UnsignedLongInt)) throw new Exception("The Object must be of UnsignedLongInt");
            UnsignedLongInt temp = (UnsignedLongInt) _number;
            if (compareWith(temp) == -1) throw new Exception("The divident must be greater than divider");
            else for (int i = 0; i < number.length() && i < temp.number.length(); i++) {
                number.set(number.get(i) - temp.number.get(i), i);
            }
            return reduceNeg();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public ILongNumber mult(ILongNumber _number) {
        try {
            if (!(_number instanceof UnsignedLongInt)) throw new Exception("The Object must be of UnsignedLongInt");
            UnsignedLongInt temp = (UnsignedLongInt) _number;
            UnsignedLongInt result = new UnsignedLongInt();
            for (int i = 0; i < temp.number.length(); i++) {
                UnsignedLongInt newNumber = new UnsignedLongInt();
                for (int j = 0; j < number.length(); j++) {
                    try {
                        newNumber.number.get(i + j);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        newNumber.number.add(0);
                    }
                    newNumber.number.set(number.get(j) * temp.number.get(i), i + j);
                }
                newNumber.reducePos();
                result.add(newNumber);
            }
            this.number = result.number;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public ILongNumber div(ILongNumber _number) {
        try {
            if (!(_number instanceof UnsignedLongInt)) throw new Exception("The Object must be of UnsignedLongInt");
            UnsignedLongInt divider = (UnsignedLongInt) _number;
            UnsignedLongInt divident = this.clone();
            number = new Array();
            number.add();
            while (divident.compareWith(divider) != -1) {
                divident.sub(divider);
                add(new UnsignedLongInt(1));
            }
            return reducePos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public ILongNumber mod(ILongNumber _number) {
        try {
            if (!(_number instanceof UnsignedLongInt)) throw new Exception("The Object must be of UnsignedLongInt");
            UnsignedLongInt divider = (UnsignedLongInt)_number;
            return sub(clone().div(divider).mult(divider));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public ILongInt shiftRight(int value) {
        String temp = toString();
        String result = "";
        for (int i = temp.length() - 1; i > temp.length() - value - 1; i--) {
            result = temp.charAt(i) + result;
        }
        for (int i = 0; i < temp.length() - value; i++) {
            result += temp.charAt(i);
        }
        number = new UnsignedLongInt(result).number;
        return this;
    }

    @Override
    public ILongInt shiftLeft(int value) {
        String temp = toString();
        String result = "";
        for (int i = value; i < temp.length(); i++) {
            result += temp.charAt(i);
        }
        for (int i = 0; i < value; i++) {
            result += temp.charAt(i);
        }
        number = new UnsignedLongInt(result).number;
        return this;
    }

    private UnsignedLongInt reducePos() {
        for (int i = 0; i < number.length(); i++) {
            int temp = number.get(i);
            if (temp >= n) {
                try {
                    number.get(i + 1);
                } catch (ArrayIndexOutOfBoundsException e) {
                    number.add(0);
                } finally {
                    number.set(temp % n, i);
                    number.set(temp / n + number.get(i + 1), i + 1);
                }
            }
        }
        return this;
    }

    private UnsignedLongInt reduceNeg() {
        if (toString().equals("0")) return this;
        for (int i = 0; i < number.length(); i++) {
            int temp = number.get(i);
            if (temp < 0) {
                try {
                    number.set(number.get(i + 1) - 1, i + 1);
                    number.set(10000 + temp, i);
                } catch (ArrayIndexOutOfBoundsException e) {
                    new Exception("Divident must be greater than divider").fillInStackTrace();
                }
            }
        }
        for (int i = number.length() - 1; number.get(i) == 0 && i != 0; i--) {
            number.delete();
        }
        return this;
    }

    @Override
    public String toString() {
        String result = "";
        boolean isFirst = true;
        for (int i = number.length() - 1; i >= 0; i--) {
            String temp = "" + number.get(i);
            if (!isFirst) {
                if (number.get(i) < 10) {
                    temp = "000" + temp;
                } else if (number.get(i) < 100) {
                    temp = "00" + temp;
                } else if (number.get(i) < 1000) {
                    temp = "0" + temp;
                }
            }
            result += temp;
            isFirst = false;
        }
        return result;
    }

    public int compareWith(UnsignedLongInt _number) {
        if (number.length() < _number.number.length()) {
            return -1;
        } else if (number.length() > _number.number.length()) {
            return 1;
        } else {
            boolean isEqual = true;
            for (int i = number.length() - 1; i >= 0; i--) {
                if (number.get(i) < _number.number.get(i)) return -1;
                else if (number.get(i) > _number.number.get(i)) return 1;
            }
            return 0;
        }
    }

    @Override
    public UnsignedLongInt clone() {
        UnsignedLongInt result = new UnsignedLongInt();
        result.number = number.clone();
        return result;
    }
}
