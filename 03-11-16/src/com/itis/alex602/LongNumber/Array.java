package com.itis.alex602.LongNumber;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Array implements ArrayInterface {
    private int[] array;

    public Array() {
        this(0);
    }

    public Array(int size) {
        array = new int[size];
    }

    public void set(int value, int index) {
        array[index] = value;
    }

    public int get(int index) {
        return array[index];
    }

    public void add() {
        add(0);
    }

    public void add(int value) {
        int[] temp = array;
        array = new int[temp.length + 1];
        for (int i = 0; i < temp.length; i++) {
            array[i] = temp[i];
        }
        array[array.length - 1] = value;
    }

    public void delete() {
        int[] temp = array;
        array = new int[temp.length - 1];
        for (int i = 0; i < array.length; i++) {
            array[i] = temp[i];
        }
    }

    public int length() {
        return array.length;
    }

    @Override 
    public Array clone() {
        Array result = new Array(array.length);
        for (int i = 0; i < array.length; i++) {
            result.array[i] = array[i];
        }
        return result;
    }
}
