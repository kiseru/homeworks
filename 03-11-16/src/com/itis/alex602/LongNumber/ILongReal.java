package com.itis.alex602.LongNumber;

/**
 * @author Alexandr Kiselev
 *         11-602
 */
public interface ILongReal extends ILongNumber {
    ILongReal ceil();
    ILongReal floor();
}
