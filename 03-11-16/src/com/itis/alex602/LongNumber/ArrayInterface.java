package com.itis.alex602.LongNumber;

/**
 * @author Alexandr Kiselev
 *         11-602
 */
public interface ArrayInterface {
    void add();
    void delete();
    int length();
}
