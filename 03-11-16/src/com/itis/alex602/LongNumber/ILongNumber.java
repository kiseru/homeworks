package com.itis.alex602.LongNumber;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public interface ILongNumber {
    ILongNumber add(ILongNumber number);
    ILongNumber sub(ILongNumber number);
    ILongNumber mult(ILongNumber number);
    ILongNumber div(ILongNumber number);
}
