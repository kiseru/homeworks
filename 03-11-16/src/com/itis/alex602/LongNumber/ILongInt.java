package com.itis.alex602.LongNumber;

/**
 * @author Alexandr Kiselev
 *         11-602
 */
public interface ILongInt extends ILongNumber {
    ILongNumber mod(ILongNumber number);
    ILongNumber shiftRight(int value);
    ILongNumber shiftLeft(int value);
}
