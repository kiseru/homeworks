package com.itis.alex602.geometry;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Rectangle extends Shape {
    private Point a;
    private Point b;
    private Point c;
    private Point d;

    public Rectangle(Point _a, Point _b) {
        a = _a.clone();
        b = new Point(_b.getX(), _a.getY());
        c = new Point(_a.getX(), _b.getY());
        d = _b.clone();
    }

    public Rectangle(Point point, double height, double width) {
        a = point.clone();
        b = new Point(a.getX() + width, a.getY());
        c = new Point(a.getX(), a.getY() - height);
        d = new Point(b.getX(), c. getY());
    }

    public double getSquare() {
        return Toolbox.abs(a.getX() - d.getX()) * Toolbox.abs(a.getY() - d.getY());
    }
}
