package com.itis.alex602.geometry;

import javax.tools.Tool;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Circle extends Shape {
    private Point point;
    private double radius;

    public Circle(Point _point, double _radius) throws Exception {
        setRadius(_radius);
        point = _point.clone();
    }

    public void setPoint(Point _point) {
        point = _point;
    }

    public Point getPoint() {
        return point;
    }

    public void setRadius(double _radius) throws Exception {
        if (radius > 0) radius = _radius;
        else throw new Exception("Radius must be greater than zero");
    }

    public double distanceTo(Point _point) {
        return Math.sqrt(Toolbox.sqr(Toolbox.abs(point.getX() - _point.getX())) + Toolbox.sqr(Toolbox.abs(point.getY() - _point.getY())));
    }
}
