package com.itis.alex602.geometry;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Toolbox {
    private Toolbox() {}

    public static double sqr(double x) {
        return x * x;
    }

    public static double abs(double x) {
        if (x < 0) x = -x;
        return x;
    }
}
