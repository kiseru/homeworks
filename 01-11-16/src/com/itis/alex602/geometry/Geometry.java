package com.itis.alex602.geometry;

import java.io.File;
import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Geometry {
    private Geometry() {}

    public static void ProcessShapes(Shape[] shapes) throws Exception {
        double square1 = 0;
        double square2 = 0;
        double square3 = 0;
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] instanceof Circle) {
                Circle temp = (Circle) shapes[i];
                Scanner scanner = new Scanner(new File("points.txt"));
                do {
                    System.out.println(temp.distanceTo(new Point(scanner.nextDouble(), scanner.nextDouble())));
                } while (scanner.hasNextInt());
                scanner.close();
            } else if (shapes[i] instanceof Rectangle) {
                Rectangle temp = (Rectangle) shapes[i];
                double square = temp.getSquare();
                if (square1 == 0) square1 = square;
                else if (square2 == 0) {
                    if (square < square1) {
                        square2 = square1;
                        square1 = square;
                    } else {
                        square2 = square;
                    }
                } else if (square3 == 0) {
                    if (square < square1) {
                        square3 = square2;
                        square2 = square1;
                        square1 = square;
                    } else if (square < square2) {
                        square3 = square2;
                        square2 = square;
                    } else {
                        square3 = square;
                    }
                } else {
                    if (square < square1) {
                        square3 = square2;
                        square2 = square1;
                        square1 = square;
                    } else if (square < square2) {
                        square3 = square2;
                        square2 = square;
                    } else if (square < square3) {
                        square3 = square;
                    }
                }
            } else if (shapes[i] instanceof Triangle) {
                Triangle temp = (Triangle) shapes[i];
                Scanner scanner = new Scanner(new File("points.txt"));
                do {
                    if (temp.isIn(new Point(scanner.nextDouble(), scanner.nextDouble())))
                        System.out.println("is in triangle");
                    else System.out.println("is out triangle");
                } while (scanner.hasNextInt());
            }
        }
        if (square1 == 0 || square2 == 0 || square3 == 0) System.out.println("Not enougth rectangles");
        else System.out.println(square1 + square2 + square3);
    }
}
