package com.itis.alex602.geometry;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    public Triangle(Point _a, Point _b, Point _c) {
        a = _a;
        b = _b;
        c = _c;
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }

    public boolean isIn(Point point) {
        double _a = (a.getX() - point.getX()) * (b.getY() - a.getY()) - (b.getX() - a.getX()) * (a.getY() - point.getY());
        double _b = (b.getX() - point.getX()) * (c.getY() - b.getY()) - (c.getX() - b.getX()) * (b.getY() - point.getY());
        double _c = (c.getX() - point.getX()) * (a.getY() - c.getY()) - (a.getX() - c.getX()) * (c.getY() - point.getY());

        if ((_a >= 0 && _b >= 0 && _c >= 0) || (_a <= 0 && _b <= 0 && _c <= 0)) return true;
        else return false;
    }
}
