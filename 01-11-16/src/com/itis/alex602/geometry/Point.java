package com.itis.alex602.geometry;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Point {
    private double x;
    private double y;

    public Point(double _x, double _y) {
        x = _x;
        y = _y;
    }

    public void setX(double _x) {
        x = _x;
    }

    public void setY(double _y) {
        y = _y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public Point clone() {
        return new Point(x, y);
    }
}
