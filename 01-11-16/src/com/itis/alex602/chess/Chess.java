package com.itis.alex602.chess;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Chess {
    private Chess() {}

    public static Colour getColourOfCell(Cell cell) throws Exception {
        return cell.getColour();
    }

    public static void pawnCanMove(Cell from, Cell to) throws Exception {
        if (from.getRow() == 1) throw new Exception("Pawn can't stand here");
        if (from.getRow() == 2 && to.getRow() == 4) System.out.println("YES");
        else if (to.getRow() - from.getRow() == 1) System.out.println("YES");
        else System.out.println("NO");
    }

    public static void knightCanMove(Cell from, Cell to) {
        if (Math.abs(from.getRow() - to.getRow()) == 2 &&
                Math.abs(from.getCol() - to.getCol()) == 1) System.out.println("YES");
        else if (Math.abs(from.getRow() - to.getRow()) == 1 &&
                Math.abs(from.getCol() - to.getCol()) == 2) System.out.println("YES");
        else System.out.println("NO");
    }

    public static void rookCanMove(Cell from, Cell to) {
        if (from.getCol() == to.getCol() || from.getRow() == to.getRow()) System.out.println("YES");
        else System.out.println("NO");
    }

    public static void isNear(Cell firstCell, Cell secondCell) {
        if (firstCell.getRow() - firstCell.getCol() == secondCell.getRow() - secondCell.getCol()
                || firstCell.getRow() - (9 - firstCell.getCol()) == secondCell.getRow() - (9 - secondCell.getCol()))
            System.out.println("SAME");
        else if (Math.abs(firstCell.getRow() - firstCell.getCol() - (secondCell.getRow() - secondCell.getCol())) == 1
                || Math.abs(firstCell.getRow() - (9 - firstCell.getCol()) - (secondCell.getRow() - (9 - secondCell.getCol()))) == 1)
            System.out.println("NEIGHBOUR");
        else System.out.println("NO");
    }
}
