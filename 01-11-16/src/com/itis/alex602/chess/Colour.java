package com.itis.alex602.chess;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public enum Colour {
    WHITE, BLACK
}
