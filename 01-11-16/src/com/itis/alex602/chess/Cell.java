package com.itis.alex602.chess;

/**
 * @author Alexandr Kiselev
 *         11-602
 */

public class Cell {
    private int row;
    private int col;
    private Colour colour;

    public Cell(int row, int col) throws Exception {
        setRow(row);
        setCol(col);
        if ((row + col) % 2 == 0) colour = Colour.BLACK;
        else colour = Colour.WHITE;
    }

    public void setRow(int _row) throws Exception {
        if (_row <= 0 || _row > 8) throw new Exception("Such row doesn't exist");
        row = _row;
    }

    public void setCol(int _col) throws Exception {
        if (_col <= 0 || _col > 8) throw new Exception("Such column doesn't exist");
        col = _col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Colour getColour() {
        return colour;
    }
}
