import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task033
 */

public class Task033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        Vector a = readVector(size);
        Vector b = readVector(size);

        System.out.println(a.dotProduct(b));
        System.out.println(a.getCosine(b));
    }

    private static Vector readVector(int size) {
        Scanner scanner = new Scanner(System.in);
        Vector vector = new Vector(size);
        for (int i = 0 ; i < size; i++) {
            vector.set(scanner.nextInt(), i);
        }
        return vector;
    }

    public static int sqr(int x) {
        return x * x;
    }
}

class Vector {
    private int[] vector;

    public Vector(int size) {
        vector = new int[size];
    }

    public void set(int value, int index) {
        vector[index] = value;
    }

    public int get(int index) {
        return vector[index];
    }

    public int dotProduct(Vector anotherVector) {
        int size = vector.length;
        int dotProduct = 0;
        for (int i = 0; i < size; i++) {
            dotProduct += this.get(i) * anotherVector.get(i);
        }
        return dotProduct;
    }

    public double length() {
        double result = 0;
        for (int i = 0; i < vector.length; i++) {
            result += Task033.sqr(get(i));
        }
        return Math.sqrt(result);
    }

    public double getCosine(Vector anotherVector) {
        return dotProduct(anotherVector) / length() / anotherVector.length();
    }
}
