import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task032
 */

public class Task032 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            a[i] += scanner.nextInt();
        }

        for (int i : a) {
            System.out.printf("%d ", i);
        }
    }
}
