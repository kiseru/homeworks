import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task035
 */

public class Task035 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        array = quickSort(array, 0, size - 1);

        for (int i : array) {
            System.out.printf("%d ", i);
        }
    }

    private static int[] quickSort(int[] array, int leftPoint, int rightPoint) {
        int i = leftPoint;
        int j = rightPoint;
        int middle = array[(leftPoint + rightPoint) / 2];
        while (i < j) {
            while (array[i] < middle) i++;
            while (array[j] > middle) j--;
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (leftPoint < j) array = quickSort(array, leftPoint, j);
        if (i < rightPoint) array = quickSort(array, i, rightPoint);
        return array;
    }
}
