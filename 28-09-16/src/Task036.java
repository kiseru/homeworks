import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task036
 */

public class Task036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int max = scanner.nextInt();
        System.out.println(max);
        for (int i = 0; i < size - 1; i++) {
            int tempInput = scanner.nextInt();
            if (tempInput > max) {
                max = tempInput;
                System.out.println(max);
            }
        }
    }
}
