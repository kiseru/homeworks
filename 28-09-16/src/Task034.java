import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task034
 */

public class Task034 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int max = a + b + c;
        for (int i = 0; i < n - 3; i++) {
            a = b;
            b = c;
            c = scanner.nextInt();
            int tempMax = a + b + c;
            if (tempMax > max) max = tempMax;
        }

        System.out.println(max);
    }
}

