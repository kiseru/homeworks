import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task037
 */

public class Task037 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int y = n / 2;
        int x0 = -(n / 2);
        boolean bool = true;
        if (n % 2 == 0) {
            y--;
            x0++;
            bool = false;
        }

        for (; y >= -(n / 2); y--) {
            for (int x = x0; x <= (n / 2); x++) {
                int input = scanner.nextInt();
                if (bool && abs(y) > abs(x)) input = 0;
                if (!bool && (y > -x && y > x - 1 || y < -x && y < x - 1)) input = 0;
                System.out.printf("%d ", input);
            }
            System.out.println();
        }
    }

    private static int abs(int number) {
        if (number < 0) return -number;
        return number;
    }
}
