/**
 * @author Alexandr Kiselev
 *          11-602
 *          ComplexNumber for Task051
 */

public class ComplexNumber {
    private double real;
    private double imaginary;

    public ComplexNumber() {
        real = 0;
        imaginary = 0;
    }

    public ComplexNumber(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public ComplexNumber add(ComplexNumber number) {
        return new ComplexNumber(real + number.real, imaginary + number.imaginary);
    }

    public void add2(ComplexNumber number) {
        real += number.real;
        imaginary += number.imaginary;
    }

    public ComplexNumber sub(ComplexNumber number) {
        return new ComplexNumber(real - number.real, imaginary - number.imaginary);
    }

    public void sub2(ComplexNumber number) {
        real -= number.real;
        imaginary -= number.imaginary;
    }

    public ComplexNumber multNumber(double k) {
        return new ComplexNumber(real * k, imaginary * k);
    }

    public void multNumber2(double k) {
        real *= k;
        imaginary *= k;
    }

    public ComplexNumber mult(ComplexNumber number) {
        return new ComplexNumber(real * number.real - imaginary * number.imaginary,
                imaginary * number.real + real * number.imaginary);
    }

    public void mult2(ComplexNumber number) {
        real = real * number.real - imaginary * number.imaginary;
        imaginary = imaginary * number.real + real * number.imaginary;
    }

    public ComplexNumber div(ComplexNumber number) {
        double real = (this.real * number.real + this.imaginary * number.imaginary);
        real /= number.real * number.real + number.imaginary * number.imaginary;
        double imaginary = (this.imaginary * number.real - this.real * number.imaginary);
        imaginary /= number.real * number.real + number.imaginary * number.imaginary;
        return new ComplexNumber(real, imaginary);
    }

    public void div2(ComplexNumber number) {
        real = (this.real * number.real + this.imaginary * number.imaginary) /
                (number.real * number.real + number.imaginary * number.imaginary);
        imaginary = (this.imaginary * number.real - this.real * number.imaginary) /
                (number.real * number.real + number.imaginary * number.imaginary);
    }

    public double length() {
        return real * real + imaginary * imaginary;
    }

    @Override
    public String toString() {
        String result = "" + imaginary + " * i ";
        if (real < 0) result += "- " + Math.abs(real);
        else result += "+ " + real;
        return result;
    }

    public double arg() {
        if (real > 0)
            return Math.atan(imaginary / real);
        else if (real < 0 && imaginary >= 0)
            return Math.atan(imaginary / real) + Math.PI;
        else if (real < 0 && imaginary < 0)
            return Math.atan(imaginary / real) - Math.PI;
        else if (real == 0 && imaginary > 0)
            return Math.PI / 2;
        else if (real == 0 && imaginary < 0)
            return -Math.PI / 2;
        else
            return 0.0 / 0;
    }

    public ComplexNumber pow(double n) {
        if (n == 0)
            return new ComplexNumber(1, 1);
        return pow(n - 1).mult(this);
    }

    public boolean equals(ComplexNumber number) {
        if (real == number.real && imaginary == number.imaginary) return true;
        return false;
    }
}
