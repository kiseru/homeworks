/**
 * @author Alexandr Kiselev
 *          11-602
 *          Vector2D for Task049
 */

public class Vector2D {
    private double[] vector;

    public Vector2D() {
        vector = new double[2];
        vector[0] = 0;
        vector[1] = 0;
    }

    public Vector2D(double x, double y) {
        vector = new double[2];
        vector[0] = x;
        vector[1] = y;
    }

    public void set(double value, int i) {
        vector[i] = value;
    }

    public void setX(double value) {
        this.set(value, 0);
    }

    public void setY(double value) {
        this.set(value, 1);
    }

    public double get(int i) {
        return vector[i];
    }

    public double getX() {
        return get(0);
    }

    public double getY() {
        return get(1);
    }

    public Vector2D add(Vector2D vector) {
        return new Vector2D(0 + vector.get(0), get(1) + vector.get(1));
    }

    public void add2(Vector2D vector) {
        this.vector[0] += vector.get(0);
        this.vector[1] += vector.get(1);
    }

    public Vector2D sub(Vector2D vector) {
        return new Vector2D(getX() - vector.getX(), getY() - vector.getY());
    }

    public void sub2(Vector2D vector) {
        this.vector[0] -= vector.getX();
        this.vector[1] -= vector.getY();
    }

    public Vector2D mult(double k) {
        return new Vector2D(getX() * k, getY() * k);
    }

    public void mult2(double k) {
        this.vector[0] *= k;
        this.vector[1] *= k;
    }

    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }

    public double length() {
        return Math.sqrt(getX() * getX() + getY() * getY());
    }

    double scalarProduct(Vector2D vector) {
        return getX() * vector.getX() + getY() * vector.getY();
    }

    public double cos(Vector2D vector) {
        return scalarProduct(vector) / length() / vector.length();
    }

    public boolean equals(Vector2D vector) {
        if (getX() == vector.getX() && getY() == vector.getY())
            return true;
        return false;
    }
}
