/**
 * @author Alexandr Kiselev
 *          11-602
 *          Matrix2x2 for Task052
 */

public class Matrix2x2 {
    private double[][] matrix;

    public Matrix2x2() {
        matrix = new double[2][2];
    }

    public Matrix2x2(double n) {
        matrix = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matrix[i][j] = n;
            }
        }
    }

    public Matrix2x2(double[][] matrix) {
        this.matrix = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }

    public Matrix2x2(double a, double b, double c, double d) {
        matrix = new double[2][2];
        matrix[0][0] = a;
        matrix[0][1] = b;
        matrix[1][0] = c;
        matrix[1][1] = d;
    }

    public void set(double value, int i, int j) {
        matrix[i][j] = value;
    }

    public double get(int i, int j) {
        return matrix[i][j];
    }

    public Matrix2x2 add(Matrix2x2 matrix) {
        Matrix2x2 result = new Matrix2x2(this.matrix);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result.matrix[i][j] += matrix.matrix[i][j];
            }
        }
        return result;
    }

    public void add2(Matrix2x2 matrix) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.matrix[i][j] += matrix.matrix[i][j];
            }
        }
    }

    public Matrix2x2 sub(Matrix2x2 matrix) {
        Matrix2x2 result = new Matrix2x2(this.matrix);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result.matrix[i][j] -= matrix.matrix[i][j];
            }
        }
        return result;
    }

    public void sub2(Matrix2x2 matrix) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.matrix[i][j] -= matrix.matrix[i][j];
            }
        }
    }

    public Matrix2x2 multNumber(double k) {
        Matrix2x2 result = new Matrix2x2(this.matrix);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result.matrix[i][j] *= k;
            }
        }
        return result;
    }

    public void multNumber2(double k) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.matrix[i][j] *= k;
            }
        }
    }

    public Matrix2x2 mult(Matrix2x2 matrix) {
        Matrix2x2 result = new Matrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    result.matrix[i][j] += this.matrix[i][k] * matrix.matrix[k][j];
                }
            }
        }
        return result;
    }

    public void mult2(Matrix2x2 matrix) {
        Matrix2x2 temp = mult(matrix);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.matrix[i][j] = temp.matrix[i][j];
            }
        }
    }

    public double det() {
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }

    public void transpon() {
        double temp = matrix[0][1];
        matrix[0][1] = matrix[1][0];
        matrix[1][0] = temp;
    }

    public Matrix2x2 inverseMatrix() {
        double d = det();
        if (d == 0) {
            System.out.println("такой матрицы не существует");
            return new Matrix2x2();
        }
        return this.multNumber(1 / d);
    }

    public Matrix2x2 equivalentDiagonal() {
        Matrix2x2 result = new Matrix2x2(matrix);
        result.set(0, 0, 1);
        result.set(result.get(1, 1) - result.get(0, 1) * result.get(1, 0), 1, 1);
        result.set(0, 1, 0);
        return result;
    }

    public Vector2D multVector(Vector2D vector) {
        Vector2D result = new Vector2D();
        for (int i = 0; i < 2; i++) {
            for (int k = 0; k < 2; k++) {
                result.set(result.get(i) + get(i, k) * vector.get(k), i);
            }
        }
        return result;
    }
}
