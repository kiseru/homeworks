import java.util.Scanner;

public class TestQueue {
    public static void main(String[] args) {
        Queue<Integer> queue = new Queue<>();
        Scanner scanner = new Scanner(System.in);
        int first = scanner.nextInt();
        int step = scanner.nextInt();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            queue.push(first);
            first += step;
        }
    }
}
