public interface ISet<T> {
    void add(T t);
    void remove(T t);
    void clear();
    boolean contains(T t);
    boolean isEmpty();
    int size();
}
