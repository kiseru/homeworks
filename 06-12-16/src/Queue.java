import java.util.LinkedList;

public class Queue<T> {
    LinkedList<T> list;

    public Queue() {
        list = new LinkedList<T>();
    }

    public void push(T t) {
        list.addLast(t);
    }

    public T pop() {
        return list.removeFirst();
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        if (size() == 0) return true;
        return false;
    }
}
