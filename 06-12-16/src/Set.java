import java.util.HashSet;

public class Set{
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        for (Integer i : set) {
            if (isPrime(i)) System.out.println(i);
        }
    }

    public static boolean isPrime(int value) {
        for (int i = 2; i < Math.sqrt(value); i++) {
            if (value % i == 0) return false;
        }
        return true;
    }
}
