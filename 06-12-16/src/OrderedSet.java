public class OrderedSet<T> implements ISet<T> {
    private Node<T> last;
    private int size;

    private static class Node<T> {
        private Node<T> next;
        private T value;

        public Node() {}

        public Node(Node next) {
            setNext(next);
        }

        public Node(Node next, T value) {
            setNext(next);
            getNext().setValue(value);
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }
    }

    public OrderedSet() {
        last = new Node<T>();
        size = 0;
    }

    @Override
    public void add(T object) {
        last = new Node<T>(last, object);
        size++;
    }

    @Override
    public void clear() {
        last = new Node<T>();
        size++;
    }

    @Override
    public boolean contains(T t) {
        boolean isIn = false;
        Node temp = last.getNext();
        for (int i = size; i > 0 && !isIn; i--) {
            if (temp.equals(t)) isIn = true;
        }
        return isIn;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) return true;
        return false;
    }

    @Override
    public void remove(Object object) {
        boolean hasDone = false;
        Node temp = last;
        for (int i = size; i > 0 && !hasDone; i--) {
            if (temp.getNext().getValue().equals(object)) {
                temp.setNext(temp.getNext().getNext());
                hasDone = true;
                size--;
            }
        }
    }

    @Override
    public int size() {
        return size;
    }
}
