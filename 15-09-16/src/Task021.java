import java.util.Scanner;

/**
 * @author  Alexandr Kiselev
 *          11-602
 *          Task021
 */

public class Task021 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        for (int y = n - 1; y >= -n; y--) {
            for (int x = -2 * n; x <= 2 * n; x++) {
                if (isInTriangle(x, y, 0, 0, n) ||
                        isInTriangle(x, y, -n, -n, n) ||
                        isInTriangle(x, y, n, -n, n)) System.out.print("*");
                else System.out.print(" ");
            }
            System.out.println();
        }
    }

    private static boolean isInTriangle(int x, int y, int p, int q, int n) {
        return y >= q && y - q <= x - p + n && y - q - n <= -x + p;
    }
}
