import java.util.Scanner;

/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task020
 */

public class Task020 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        for (int y = n; y >= -n; y--) {
            for (int x = -n; x <= n; x++) {
                if (x + n >= y && x - n <= y && -x - n <= y && -x + n >= y) System.out.print("0");
                else System.out.print("*");
            }
            System.out.println();
        }
    }

    private static int abs(int x) {
        if (x < 0) return -x;
        return x;
    }
}
