/**
 * @author Alexandr Kiselev
 *          11-602
 *          Task019
 */

public class Task019 {
    public static void main(String[] args) {
        for (int i = 1; i <= 1000000; i++) {
            if (isPerfectNumber(i)) System.out.println(i);
        }
    }

    private static boolean isPerfectNumber(int number) {
        int sum = 1;
        for (int i = 2; i <= Math.sqrt(number) && sum <= number; i++) {
            if (number % i != 0)
                continue;

            sum += i;
            sum += number / i;
        }
        if (sum == number && sum != 1) return true;
        return false;
    }
}
