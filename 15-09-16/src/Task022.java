import java.util.Scanner;

/**
 * @author  Kiselev Alexandr
 *          11-602
 *          Task022
 */

public class Task022 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        drawCircle(n);
    }

    private static void drawCircle(int radius) {
        for (int i = -radius; i <= radius; i++) {
            for (int j = radius; j >= -radius; j--) {
                if (isElementOfCircle(i, j, radius))
                    System.out.print("0");
                else
                    System.out.print("*");
            }
            System.out.println();
        }
    }

    private static boolean isElementOfCircle(int x, int y, int r) {
        if (sqr(x) + sqr(y) <= sqr(r)) return true;
        return false;
    }

    private static int sqr(int a) {
        return a * a;
    }
}
